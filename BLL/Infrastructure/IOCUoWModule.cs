﻿using Ninject.Modules;
using DAL.Interfaces;
using DAL.CRUD;

namespace BLL.Infrastructure
{
    /// <summary>
    /// Class for creating container for unit of work
    /// </summary>
    public class IOCUoWModule : NinjectModule
    {
        /// <summary>
        /// Connection string of DB 
        /// </summary>
        private string ConectionString;

        /// <summary>
        /// The constructor for IOC container for DB
        /// </summary>
        /// <param name="conectionString">connection string for DB</param>
        public IOCUoWModule(string conectionString)
        {
            ConectionString = conectionString;
        }

        /// <summary>
        /// To load needed classes in container
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(ConectionString);
        }

    }
}
