﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Infrastructure
{
    /// <summary>
    /// The class of own exception 
    /// </summary>
    public class ValidationException : Exception
    {
        /// <summary>
        /// The property name that caused the exception throwing
        /// </summary>
        public string Property { get; protected set; }

        /// <summary>
        /// The constructor for throwing exception
        /// </summary>
        /// <param name="message">message of exception</param>
        /// <param name="property">name of property that caused the exception throwing</param>
        public ValidationException(string message, string property) : base(message)
        {
            Property = property;
        }
    }
}
