﻿using System;
using System.Collections.Generic;
using BLL.EntetiesDTO;

namespace BLL.Interfaces
{
    public interface IPriceListService
    {
        /// <summary>
        /// To add new price for the dish 
        /// </summary>
        /// <param name="priceDto">the instance of new price for the dish</param>
        void Add(PriceListDto priceDto);

        /// <summary>
        /// To get all prices for all dishes
        /// </summary>
        /// <returns>enumerable collection of prices for dishes</returns>
        IEnumerable<PriceListDto> GetPrices();

        /// <summary>
        /// To delete price for the dish 
        /// </summary>
        /// <param name="id">id of price for dish that will be deleted </param>
        void Delete(int id);

        /// <summary>
        /// To update information about one price for dish 
        /// </summary>
        /// <param name="priceListDto">the instance of price for dish that will be updated with new information</param>
        void Update(PriceListDto priceListDto);

        /// <summary>
        /// To dispose DB context manualy
        /// </summary>
        void Dispose();
    }
}
