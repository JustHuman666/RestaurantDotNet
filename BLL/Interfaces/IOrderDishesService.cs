﻿using System.Collections.Generic;
using BLL.EntetiesDTO;

namespace BLL.Interfaces
{
    public interface IOrderDishesService
    {
        /// <summary>
        /// To add new dish in order
        /// </summary>
        /// <param name="OrderDishesDto">the instance of new dish in order</param>
        void Add(OrderDishesDto OrderDishesDto);

        /// <summary>
        /// To find dishes in order by this order id
        /// </summary>
        /// <param name="orderId">the id of order</param>
        /// <returns>enumerable collection of dishes in this order</returns>
        IEnumerable<OrderDishesDto> FindOrderDishesByOrderId(int orderId);


        /// <summary>
        /// To get all dishes if all orders from DB
        /// </summary>
        /// <returns>enumerable collection of dishes of orders</returns>
        IEnumerable<OrderDishesDto> GetOrdersDishes();

        /// <summary>
        /// To delete dish from order
        /// </summary>
        /// <param name="id">id of dish that will be deleted from order</param>
        void Delete(int id);

        /// <summary>
        /// To update information about one dish in order
        /// </summary>
        /// <param name="OrderDishesDto">the instance of dish in order that will be updated with new information</param>
        void Update(OrderDishesDto OrderDishesDto);

        /// <summary>
        /// To dispose DB context manualy
        /// </summary>
        void Dispose();
    }
}
