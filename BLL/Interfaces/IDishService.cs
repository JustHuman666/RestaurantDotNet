﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.EntetiesDTO;

namespace BLL.Interfaces
{
    public interface IDishService
    {
        /// <summary>
        /// To add new dish in DB
        /// </summary>
        /// <param name="DishDto">the instance of new dish that will be added</param>
        void Add(DishDto DishDto);

        /// <summary>
        /// To find dish by its name
        /// </summary>
        /// <param name="name">the name of searching dish</param>
        /// <returns>the instance of dish</returns>
        DishDto FindByName(string name);

        /// <summary>
        /// To get all dishes from DB
        /// </summary>
        /// <returns>enumerable collection of dishes</returns>
        IEnumerable<DishDto> GetDishes();

        /// <summary>
        /// To delete dish from DB
        /// </summary>
        /// <param name="id">id of dish that will be deleted</param>
        void Delete(int id);

        /// <summary>
        /// To update the dish information in DB
        /// </summary>
        /// <param name="DishDto">the instance of dish that will be updated with new information</param>
        void Update(DishDto DishDto);

        /// <summary>
        /// To add new ingredient to the dish
        /// </summary>
        /// <param name="dishId">the id of dish where ingridient will be added</param>
        /// <param name="ingredientId">the id of ingridient that will be added</param>
        void AddIngredient(int dishId, int ingredientId);

        /// <summary>
        /// To delete ingredient from chosen dish
        /// </summary>
        /// <param name="dishId">the id of dish where ingridient will be deleted from</param>
        /// <param name="ingredientId">the id of ingridient that will be deleted</param>
        void DeleteIngredient(int dishId, int ingredientId);

        /// <summary>
        /// To get the list of all ingredients of one dish
        /// </summary>
        /// <param name="dishId">the id of dish where ingridients will be shown</param>
        /// <returns>enumerable collection of ingredients</returns>
        IEnumerable<IngredientDto> GetIngredientsByDishId(int dishId);

        /// <summary>
        /// To dispose DB context manualy
        /// </summary>
        void Dispose();
    }
}
