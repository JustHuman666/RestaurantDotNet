﻿using System;
using System.Collections.Generic;
using BLL.EntetiesDTO;

namespace BLL.Interfaces
{
    public interface IOrderService
    {
        /// <summary>
        /// To add new order
        /// </summary>
        /// <param name="OrderDto">the instance of new order</param>
        void Add(OrderDto OrderDto);

        /// <summary>
        /// To find order by customer name
        /// </summary>
        /// <param name="name">the name of customer</param>
        /// <returns>enumerable collection of orders for this customer</returns>
        IEnumerable<OrderDto> FindOrdersByCustomer(string name);

        /// <summary>
        /// To get all orders from DB
        /// </summary>
        /// <returns>enumerable collection orders</returns>
        IEnumerable<OrderDto> GetOrders();

        /// <summary>
        /// To delete order from DB
        /// </summary>
        /// <param name="id">id of order that will be deleted</param>
        void Delete(int id);

        /// <summary>
        /// To update information about one order
        /// </summary>
        /// <param name="OrderDto">the instance oforder that will be updated with new information</param>
        void Update(OrderDto OrderDto);

        /// <summary>
        /// To add new dish to chosen order
        /// </summary>
        /// <param name="orderId">the order id where dish will be added to</param>
        /// <param name="priceId">the id of dish and price for it that will be added</param>
        /// <param name="amount">the amount of portions that will be added</param>
        void AddDishToOrder(int orderId, int priceId, int amount);

        /// <summary>
        /// To dispose DB context manualy
        /// </summary>
        void Dispose();
    }
}
