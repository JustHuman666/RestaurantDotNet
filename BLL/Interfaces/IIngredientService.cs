﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.EntetiesDTO;

namespace BLL.Interfaces
{
    public interface IIngredientService
    {
        /// <summary>
        /// To add new dish in DB
        /// </summary>
        /// <param name="ingredientDto">the instance of new ingredient that will be added</param>
        void Add(IngredientDto ingredientDto);

        /// <summary>
        /// To find ingredient by its name
        /// </summary>
        /// <param name="name">the name of searching ingredient</param>
        /// <returns>the instance of ingredient</returns>
        IngredientDto FindByName(string name);

        /// <summary>
        /// To get all ingredients from DB
        /// </summary>
        /// <returns>enumerable collection of ingredients</returns>
        IEnumerable<IngredientDto> GetIngredients();

        /// <summary>
        /// To delete ingredient from DB
        /// </summary>
        /// <param name="id">id of ingredient that will be deleted</param>
        void Delete(int id);

        /// <summary>
        /// To update the ingredient information in DB
        /// </summary>
        /// <param name="ingredientDto">the instance of ingredient that will be updated with new information</param>
        void Update(IngredientDto ingredientDto);

        /// <summary>
        /// To dispose DB context manualy
        /// </summary>
        void Dispose();
    }
}
