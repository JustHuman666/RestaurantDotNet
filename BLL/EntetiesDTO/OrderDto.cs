﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.EntetiesDTO
{
    public class OrderDto
    {
        /// <summary>
        /// The id of order
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Total price of this order
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Name of customer for this order
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Date of creating of order
        /// </summary>
        public DateTime OrderDate { get; set; }
    }
}
