﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.EntetiesDTO
{
    public class DishDto
    {
        /// <summary>
        /// An id of dish
        /// </summary>
        public int DishId { get; set; }

        /// <summary>
        /// The name of dish
        /// </summary>
        public string DishName { get; set; }

    }
}
