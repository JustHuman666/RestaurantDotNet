﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.EntetiesDTO;
using BLL.Infrastructure;
using DAL.Enteties;
using AutoMapper;
using DAL.Interfaces;
using BLL.Interfaces;

namespace BLL.Services
{
    public class DishService : IDishService
    {
        /// <summary>
        /// The unit of work which present the DB
        /// </summary>
        private readonly IUnitOfWork db;

        /// <summary>
        /// The constructor with given unit of work
        /// </summary>
        /// <param name="unitOfWork">instance of unit of work</param>
        public DishService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }

        public void Add(DishDto DishDto)
        {
            if (DishDto == null)
            {
                throw new ArgumentNullException($"{nameof(DishDto)} null exception");
            }
            if (DishDto.DishName == "")
            {
                throw new ValidationException("Name cannot be empty", nameof(DishDto.DishName));
            }
            var dish = db.Dishes.GetAll().FirstOrDefault(d => d.DishName == DishDto.DishName);
            if (dish != null)
            {
                throw new ValidationException("Such dish is already exist", nameof(dish));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishDto, Dish>()).CreateMapper();
            db.Dishes.Create(mapper.Map<DishDto, Dish>(DishDto));
            db.Save();
        }


        public DishDto FindByName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException($"{nameof(name)} null exception");
            }
            if (name == "")
            {
                throw new ValidationException("The name cannot be empty", nameof(name));
            }
            var dish = db.Dishes.GetAll().FirstOrDefault(d => d.DishName == name);
            if (dish == null)
            {
                throw new ValidationException("There is not such dish", nameof(dish));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Dish, DishDto>()).CreateMapper();
            return mapper.Map<Dish, DishDto>(dish);
        }

        public IEnumerable<DishDto> GetDishes()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Dish, DishDto>()).CreateMapper();
            var dishes = db.Dishes.GetAll();
            if (dishes.Count() == 0)
            {
                throw new ValidationException("There is not any dish", nameof(dishes));
            }
            var dishesDto = mapper.Map<IEnumerable<Dish>, IEnumerable<DishDto>>(dishes);
            return dishesDto;
        }

        public void Delete(int id)
        {
            if (db.Dishes.SearchById(id) == null)
            {
                throw new ValidationException("Such dish doesn't exist", nameof(id));
            }
            db.Dishes.Delete(id);
            db.Save();
        }

        public void Update(DishDto DishDto)
        {
            if (DishDto == null)
            {
                throw new ArgumentNullException($"{nameof(DishDto)} null exception");
            }
            if (DishDto.DishName == "")
            {
                throw new ValidationException("Name cannot be empty", nameof(DishDto.DishName));
            }
            var dish = db.Dishes.GetAll().FirstOrDefault(d => d.DishId == DishDto.DishId);
            if (dish == null)
            {
                throw new ValidationException("Such dish does not exist", nameof(dish));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishDto, Dish>()).CreateMapper();
            db.Dishes.Update(mapper.Map<DishDto, Dish>(DishDto));
            db.Save();
        }

        public void AddIngredient(int dishId, int ingredientId)
        {
            var dish = db.Dishes.SearchById(dishId);
            var ingredient = db.Ingredients.SearchById(ingredientId);
            if(dish == null || ingredient == null)
            {
                throw new ValidationException("Such dish or ingredient does not exist", nameof(ingredient) + " " + nameof(dish));
            }
            if(dish.Ingredients == null)
            {
                dish.Ingredients = new HashSet<Ingredient>();
            }
            dish.Ingredients.Add(ingredient);
            db.Dishes.Update(dish);
            db.Save();
        }

        public void DeleteIngredient(int dishId, int ingredientId)
        {
            var dish = db.Dishes.SearchById(dishId);
            var ingredient = db.Ingredients.SearchById(ingredientId);
            if (dish == null || ingredient == null)
            {
                throw new ValidationException("Such dish or ingredient does not exist", nameof(ingredient) + " " + nameof(dish));
            }
            if(dish.Ingredients.Count == 0)
            {
                throw new ValidationException("Cannot delete from empty list", nameof(dish.Ingredients.Count));
            }
            dish.Ingredients.Remove(ingredient);
            db.Dishes.Update(dish);
            db.Save();
        }

        public IEnumerable<IngredientDto> GetIngredientsByDishId(int dishId)
        {
            var dish = db.Dishes.SearchById(dishId);
            if (dish == null)
            {
                throw new ValidationException("Such dish does not exist", nameof(dish));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Ingredient, IngredientDto>()).CreateMapper();
            var ingredients = mapper.Map<IEnumerable<Ingredient>, IEnumerable<IngredientDto>>(dish.Ingredients);
            if(ingredients.Count() == 0)
            {
                throw new ValidationException("There is not any ingredient for this dish", nameof(ingredients));
            }
            return ingredients;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
