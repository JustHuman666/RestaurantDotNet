﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BLL.EntetiesDTO;
using BLL.Infrastructure;
using DAL.Enteties;
using DAL.CRUD;
using AutoMapper;
using DAL.Interfaces;
using BLL.Interfaces;

namespace BLL.Services
{
    public class OrderService : IOrderService
    {
        /// <summary>
        /// The unit of work which present the DB
        /// </summary>
        private readonly IUnitOfWork db;

        /// <summary>
        /// The unit of work which present the DB
        /// </summary>
        public OrderService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }

        public void Add(OrderDto OrderDto)
        {
            if (OrderDto == null)
            {
                throw new ArgumentNullException($"{nameof(OrderDto)} null exception");
            }
            if (OrderDto.OrderDate == default || OrderDto.CustomerName == "")
            {
                throw new ValidationException("Values cannot be empty", nameof(OrderDto));
            }
            if (db.Orders.GetAll().Any(ord => ord.CustomerName == OrderDto.CustomerName && ord.OrderDate == OrderDto.OrderDate && ord.TotalPrice == OrderDto.TotalPrice))
            {
                throw new ValidationException("Such order is alredy exist", nameof(OrderDto));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDto, Order>()).CreateMapper();
            db.Orders.Create(mapper.Map<OrderDto, Order>(OrderDto));
            db.Save();
        }

        public IEnumerable<OrderDto> FindOrdersByCustomer(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException($"{nameof(name)} null exception");
            }
            if (name == "")
            {
                throw new ValidationException("The name cannot be empty", nameof(name));
            }
            var orders = db.Orders.GetAll().Where(d => d.CustomerName == name);
            if(orders.Count() == 0)
            {
                throw new ValidationException("This customer doesn't have ane order", nameof(orders));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(orders);
        }

        public IEnumerable<OrderDto> GetOrders()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDto>()).CreateMapper();
            var orders = db.Orders.GetAll();
            if(orders.Count() == 0)
            {
                throw new ValidationException("There is not any order", nameof(orders));
            }
            var ordersDto = mapper.Map<IEnumerable<Order>, IEnumerable<OrderDto>>(orders);
            return ordersDto;
        }

        public void Delete(int id)
        {
            if (db.Orders.SearchById(id) == null)
            {
                throw new ValidationException("Such order doesn't exist", nameof(id));
            }
            db.Orders.Delete(id);
            db.Save();
        }

        public void Update(OrderDto OrderDto)
        {
            if (OrderDto == null)
            {
                throw new ArgumentNullException($"{nameof(OrderDto)} null exception");
            }
            if (OrderDto.OrderDate == default || OrderDto.CustomerName == "")
            {
                throw new ValidationException("Values cannot be empty", nameof(OrderDto));
            }
            var order = db.Orders.GetAll().FirstOrDefault(d => d.OrderId == OrderDto.OrderId);
            if (order == null)
            {
                throw new ValidationException("Such order does not exist", nameof(order));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDto, Order>()).CreateMapper();
            db.Orders.Update(mapper.Map<OrderDto, Order>(OrderDto));
            db.Save();
        }

        public void AddDishToOrder(int orderId, int priceId, int amount)
        {
            if(amount < 1 || amount > 200)
            {
                throw new ValidationException("Impossible to add dish with such amount", nameof(amount));
            }
            var order = db.Orders.SearchById(orderId);
            var price = db.Prices.SearchById(priceId);
            if (order == null || price == null)
            {
                throw new ValidationException("Such dish or order does not exist", nameof(price) + " " + nameof(order));
            }
            if(order.DishesList == null)
            {
                order.DishesList = new HashSet<OrderDishes>();
            }
            order.DishesList.Add(new OrderDishes() { OrderId = orderId, PriceId = priceId, PortionCount = amount});
            db.Save();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
