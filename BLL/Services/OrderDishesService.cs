﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BLL.EntetiesDTO;
using BLL.Infrastructure;
using DAL.Enteties;
using DAL.CRUD;
using AutoMapper;
using DAL.Interfaces;
using BLL.Interfaces;

namespace BLL.Services
{
    public class OrderDishesService : IOrderDishesService
    {
        /// <summary>
        /// The unit of work which present the DB
        /// </summary>
        private readonly IUnitOfWork db;

        /// <summary>
        /// The constructor with given unit of work
        /// </summary>
        /// <param name="unitOfWork">instance of unit of work</param>
        public OrderDishesService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }

        public void Add(OrderDishesDto OrderDishesDto)
        {
            if (OrderDishesDto == null)
            {
                throw new ArgumentNullException($"{nameof(OrderDishesDto)} null exception");
            }
            if(OrderDishesDto.OrderId == default || OrderDishesDto.PriceId == default || OrderDishesDto.PortionCount == default)
            {
                throw new ValidationException("Values cannot be empty", nameof(OrderDishesDto));
            }
            if (db.DishesInOrder.GetAll().Any(ord => ord.OrderId == OrderDishesDto.OrderId && ord.PriceId == OrderDishesDto.PriceId && ord.PortionCount == OrderDishesDto.PortionCount))
            {
                throw new ValidationException("Such dish with such amount is alredy exist in this order", nameof(OrderDishesDto));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesDto, OrderDishes>()).CreateMapper();
            db.DishesInOrder.Create(mapper.Map<OrderDishesDto, OrderDishes>(OrderDishesDto));
            db.Save();
        }

        public IEnumerable<OrderDishesDto> FindOrderDishesByOrderId(int orderId)
        {
            var orderDishes = db.DishesInOrder.GetAll().Where(ord => ord.OrderId == orderId);
            if (orderDishes.Count() == 0)
            {
                throw new ValidationException("This order does not has any dishes", nameof(orderDishes));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishes, OrderDishesDto>()).CreateMapper();
            return mapper.Map<IEnumerable<OrderDishes>, IEnumerable<OrderDishesDto>>(orderDishes);
        }

        public IEnumerable<OrderDishesDto> GetOrdersDishes()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishes, OrderDishesDto>()).CreateMapper();
            var orderDishes = mapper.Map<IEnumerable<OrderDishes>, IEnumerable<OrderDishesDto>>(db.DishesInOrder.GetAll());
            if(orderDishes.Count() == 0)
            {
                throw new ValidationException("There does not exist any dish", nameof(orderDishes));
            }
            return orderDishes;
        }

        public void Delete(int id)
        {
            var dishInOrder = db.DishesInOrder.SearchById(id);
            if (dishInOrder == null)
            {
                throw new ValidationException("Such dish in order doesn't exist", nameof(id));
            }
            db.DishesInOrder.Delete(id);
            db.Save();
        }

        public void Update(OrderDishesDto OrderDishesDto)
        {
            if (OrderDishesDto == null)
            {
                throw new ArgumentNullException($"{nameof(OrderDishesDto)} null exception");
            }
            if (OrderDishesDto.OrderId == default || OrderDishesDto.PriceId == default || OrderDishesDto.PortionCount == default)
            {
                throw new ValidationException("Values cannot be empty", nameof(OrderDishesDto));
            }
            var orderDish = db.DishesInOrder.GetAll().FirstOrDefault(d => d.OrderDishesId == OrderDishesDto.OrderDishesId);
            if (orderDish == null)
            {
                throw new ValidationException("Such order does not exist", nameof(orderDish));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesDto, OrderDishes>()).CreateMapper();
            db.DishesInOrder.Update(mapper.Map<OrderDishesDto, OrderDishes>(OrderDishesDto));
            db.Save();
        }

        
        public void Dispose()
        {
            db.Dispose();
        }
    }
}
