﻿using System;
using System.Collections.Generic;
using BLL.Interfaces;
using System.Linq;
using BLL.EntetiesDTO;
using BLL.Infrastructure;
using DAL.Enteties;
using DAL.CRUD;
using AutoMapper;
using DAL.Interfaces;

namespace BLL.Services
{
    public class PriceListService : IPriceListService
    {
        /// <summary>
        /// The unit of work which present the DB
        /// </summary>
        private readonly IUnitOfWork db;

        /// <summary>
        /// The constructor with given unit of work
        /// </summary>
        /// <param name="unitOfWork">instance of unit of work</param>
        public PriceListService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }

        public void Add(PriceListDto priceDto)
        {
            if (priceDto == null)
            {
                throw new ArgumentNullException($"{nameof(priceDto)} null exception");
            }
            if (priceDto.DishId == default || priceDto.Portion == default || priceDto.PricePerPortion == default)
            {
                throw new ValidationException("Values cannot be empty", nameof(priceDto));
            }
            if (db.Prices.GetAll().Any(pr => pr.DishId == priceDto.DishId && pr.Portion == priceDto.Portion))
            {
                throw new ValidationException("Such price is already exist", nameof(priceDto));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceListDto, PriceList>()).CreateMapper();
            db.Prices.Create(mapper.Map<PriceListDto, PriceList>(priceDto));
            db.Save();
        }

        public IEnumerable<PriceListDto> GetPrices()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceList, PriceListDto>()).CreateMapper();
            var prices = mapper.Map<IEnumerable<PriceList>, IEnumerable<PriceListDto>>(db.Prices.GetAll());
            if(prices.Count() == 0)
            {
                throw new ValidationException("There is not any prices", nameof(prices));
            }
            return prices;
        }

        public void Delete(int id)
        {
            if (db.Prices.SearchById(id) == null)
            {
                throw new ValidationException("Such price doesn't exist", nameof(id));
            }
            db.Prices.Delete(id);
            db.Save();
        }

        public void Update(PriceListDto priceListDto)
        {
            if (priceListDto == null)
            {
                throw new ArgumentNullException($"{nameof(priceListDto)} null exception");
            }
            if (priceListDto.DishId == default || priceListDto.Portion == default || priceListDto.PricePerPortion == default)
            {
                throw new ValidationException("Values cannot be empty", nameof(priceListDto));
            }
            var priceList = db.Prices.GetAll().FirstOrDefault(pr => pr.PriceId == priceListDto.PriceId);
            if (priceList == null)
            {
                throw new ValidationException("Such price does not exist", nameof(priceList));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceListDto, PriceList>()).CreateMapper();
            db.Prices.Update(mapper.Map<PriceListDto, PriceList>(priceListDto));
            db.Save();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
