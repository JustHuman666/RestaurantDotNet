﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BLL.EntetiesDTO;
using BLL.Infrastructure;
using DAL.Enteties;
using DAL.CRUD;
using AutoMapper;
using DAL.Interfaces;
using BLL.Interfaces;

namespace BLL.Services
{
    public class IngredientService : IIngredientService
    {
        /// <summary>
        /// The unit of work which present the DB
        /// </summary>
        private readonly IUnitOfWork db;

        /// <summary>
        /// The constructor with given unit of work
        /// </summary>
        /// <param name="unitOfWork">instance of unit of work</param>
        public IngredientService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }

        public void Add(IngredientDto ingredientDto)
        {
            if(ingredientDto == null)
            {
                throw new ArgumentNullException($"{nameof(ingredientDto)} null exception");
            }
            if (ingredientDto.IngredientName == "")
            {
                throw new ValidationException("Name cannot be empty", nameof(ingredientDto.IngredientName));
            }
            var ingredient = db.Ingredients.GetAll().FirstOrDefault(ingr => ingr.IngredientName == ingredientDto.IngredientName);
            if(ingredient != null)
            {
                throw new ValidationException("Such ingredient is already exist", nameof(ingredient));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, Ingredient>()).CreateMapper();
            db.Ingredients.Create(mapper.Map<IngredientDto, Ingredient>(ingredientDto));
            db.Save();
        }

        public IngredientDto FindByName(string name)
        {
            if(name == null)
            {
                throw new ArgumentNullException($"{nameof(name)} null exception");
            }
            if(name == "")
            {
                throw new ValidationException("The name cannot be empty", nameof(name));
            }
            var ingredient = db.Ingredients.GetAll().FirstOrDefault(ingr => ingr.IngredientName == name);
            if (ingredient == null)
            {
                throw new ValidationException("There is not such ingredient", nameof(ingredient));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Ingredient, IngredientDto>()).CreateMapper();
            var ingredientDto = mapper.Map<Ingredient, IngredientDto>(ingredient);
            return ingredientDto;
        }

        public IEnumerable<IngredientDto> GetIngredients()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Ingredient, IngredientDto>()).CreateMapper();
            var ingredients = mapper.Map<IEnumerable<Ingredient>, IEnumerable<IngredientDto>>(db.Ingredients.GetAll());
            if (ingredients.Count() == 0)
            {
                throw new ValidationException("There is not any ingredients", nameof(ingredients));
            }
            return ingredients;
        }

        public void Delete(int id)
        {
            if(db.Ingredients.SearchById(id) == null)
            {
                throw new ValidationException("Such ingredient doesn't exist", nameof(id));
            }
            db.Ingredients.Delete(id);
            db.Save();
        }

        public void Update(IngredientDto ingredientDto)
        {
            if (ingredientDto == null)
            {
                throw new ArgumentNullException($"{nameof(ingredientDto)} null exception");
            }
            if (ingredientDto.IngredientName == "")
            {
                throw new ValidationException("Name cannot be empty", nameof(ingredientDto.IngredientName));
            }
            var ingredient = db.Ingredients.GetAll().FirstOrDefault(ingr => ingr.IngredientId == ingredientDto.IngredientId);
            if (ingredient == null)
            {
                throw new ValidationException("Such ingredient does not exist", nameof(ingredient));
            }
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, Ingredient>()).CreateMapper();
            db.Ingredients.Update(mapper.Map<IngredientDto, Ingredient>(ingredientDto));
            db.Save();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
