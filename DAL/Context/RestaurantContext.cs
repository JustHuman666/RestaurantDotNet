﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Enteties;
using Microsoft.EntityFrameworkCore;

namespace DAL.Context
{
    public class RestaurantContext : DbContext
    {
        /// <summary>
        /// DB Set of ingredients 
        /// </summary>
        public DbSet<Ingredient> Ingredients { get; set; }

        /// <summary>
        /// DB Set of dishes
        /// </summary>
        public DbSet<Dish> Dishes { get; set; }

        /// <summary>
        /// DB Set of orders
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// DB Set of prices for dishes
        /// </summary>
        public DbSet<PriceList> PriceLists { get; set; }

        /// <summary>
        /// DB Set of dishes in orders
        /// </summary>
        public DbSet<OrderDishes> OrderDishes { get; set; }

        /// <summary>
        /// String of connection to DB
        /// </summary>
        private string ConectionString { get; set; }

        /// <summary>
        /// Connecting to the DB
        /// </summary>
        /// <param name="optionsBuilder">option builder for DB for connecting</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConectionString);
        }

        /// <summary>
        /// Constructor for creating of DB context from DB
        /// </summary>
        /// <param name="conection">string of connection to DB</param>
        public RestaurantContext(string conection)
        {
            ConectionString = conection;
        }
    }
}
