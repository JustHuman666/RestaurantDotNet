﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Enteties;
using DAL.Context;
using DAL.CRUD;


namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// CRUD repository for work with ingredients
        /// </summary>
        IRepository<Ingredient> Ingredients { get; }

        /// <summary>
        /// CRUD repository for work with dishes
        /// </summary>
        IRepository<Dish>  Dishes { get; }

        /// <summary>
        /// CRUD repository for work with orders
        /// </summary>
        IRepository<Order> Orders { get; }

        /// <summary>
        /// CRUD repository for work with prices for dishes
        /// </summary>
        IRepository<PriceList> Prices { get; }

        /// <summary>
        /// CRUD repository for work with dishes in orders
        /// </summary>
        IRepository<OrderDishes> DishesInOrder { get; }

        /// <summary>
        /// To call disposeïng of DB context manualy
        /// </summary>
        void Dispose();

        /// <summary>
        /// To save changes in DB
        /// </summary>
        void Save();
    }
}
