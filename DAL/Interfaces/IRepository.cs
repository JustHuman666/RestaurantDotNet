﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// To get item from DB
        /// </summary>
        /// <param name="id">id of item that is searching for</param>
        T SearchById(int id);

        /// <summary>
        /// To get all items of such type from DB
        /// </summary>
        /// <returns>enumerable collection of all items of such type from DB</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// To add new item in DB
        /// </summary>
        /// <param name="item">instance of item that will be added to db</param>
        /// <returns>true if done, or false if not</returns>
        bool Create(T item);

        /// <summary>
        /// To update the item in DB
        /// </summary>
        /// <param name="item">instance of item that will be updated in db</param>
        /// <returns>true if done, or false if not</returns>
        bool Update(T item);

        /// <summary>
        /// To delete the item from DB
        /// </summary>
        /// <param name="item">instance of item that will be deleted from db</param>
        /// <returns>true if done, or false if not</returns>
        bool Delete(int id);

    }
}
