﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Context;
using DAL.Enteties;
using DAL.Interfaces;

namespace DAL.CRUD
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly RestaurantContext restContext;

        private IngredientRepository ingredientRepo;

        private DishRepository dishRepo;

        private OrderRepository orderRepo;

        private PriceListRepository priceRepo;

        private OrderDishesRepository orderDishesRepo;

        /// <summary>
        /// Constructor for unit of work for connecting to DB
        /// </summary>
        /// <param name="conectionString">string of connection to DB</param>
        public UnitOfWork(string conectionString)
        {
            restContext = new RestaurantContext(conectionString);
        }

        public IRepository<Dish> Dishes
        {
            get
            {
                if (dishRepo == null)
                {
                    dishRepo = new DishRepository(restContext);
                }
                return dishRepo;
            }
        }

        public IRepository<Ingredient> Ingredients
        {
            get
            {
                if (ingredientRepo == null)
                {
                    ingredientRepo = new IngredientRepository(restContext);
                }
                return ingredientRepo;
            }
        }

        public IRepository<OrderDishes> DishesInOrder
        {
            get
            {
                if (orderDishesRepo == null)
                {
                    orderDishesRepo = new OrderDishesRepository(restContext);
                }
                return orderDishesRepo;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (orderRepo == null)
                {
                    orderRepo = new OrderRepository(restContext);
                }
                return orderRepo;
            }
        }

        public IRepository<PriceList> Prices
        {
            get
            {
                if(priceRepo == null)
                {
                    priceRepo = new PriceListRepository(restContext);
                }
                return priceRepo;
            }
        }

        public void Save()
        {
            restContext.SaveChanges();
        }

        private bool disposed = false;

        /// <summary>
        /// Method of disposing of this DB context
        /// </summary>
        /// <param name="disposing">bool value to confirm the disposing</param>
        public virtual void Dispose(bool disposing)
        {
            if(!disposed)
            {
                if(disposing)
                {
                    restContext.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
