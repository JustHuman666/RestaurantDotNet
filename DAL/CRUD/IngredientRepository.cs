﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Context;
using DAL.Enteties;
using DAL.Interfaces;
using System.Linq;

namespace DAL.CRUD
{
    public class IngredientRepository : IRepository<Ingredient>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly RestaurantContext restContext;

        /// <summary>
        /// Constructor of repository with given context
        /// </summary>
        /// <param name="context">context of DB</param>
        public IngredientRepository(RestaurantContext context)
        {
            restContext = context;
        }

        public bool Create(Ingredient item)
        {
            if(item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            if(restContext.Ingredients.Find(item.IngredientId) != null)
            {
                return false;
            }
            restContext.Ingredients.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var ingredient = restContext.Ingredients.Find(id);
            if (ingredient == null)
            {
                return false;
            }
            restContext.Ingredients.Remove(ingredient);
            return true;
        }

        public IEnumerable<Ingredient> GetAll()
        {
            return restContext.Ingredients;
        }

        public Ingredient SearchById(int id)
        {
            return restContext.Ingredients.Find(id);
        }

        public bool Update(Ingredient item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            var ingredient = restContext.Ingredients.Find(item.IngredientId);
            if (ingredient == null)
            {
                return false;
            }
            ingredient.IngredientName = item.IngredientName;
            return true;
        }

    }
}
