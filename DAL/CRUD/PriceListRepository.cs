﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DAL.Context;
using DAL.Enteties;
using DAL.Interfaces;

namespace DAL.CRUD
{
    public class PriceListRepository : IRepository<PriceList>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly RestaurantContext restContext;

        /// <summary>
        /// Constructor of repository with given context
        /// </summary>
        /// <param name="context">context of DB</param>
        public PriceListRepository(RestaurantContext context)
        {
            restContext = context;
        }

        public bool Create(PriceList item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            var priceList = restContext.PriceLists.Find(item.PriceId);
            if (priceList != null)
            {
                return false;
            }
            restContext.PriceLists.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var price = restContext.PriceLists.Find(id);
            if ( price == null)
            {
                return false;
            }
            restContext.PriceLists.Remove(price);
            return true;
        }

        public IEnumerable<PriceList> GetAll()
        {
            return restContext.PriceLists;
        }

        public PriceList SearchById(int id)
        {
            return restContext.PriceLists.Find(id);
        }

        public bool Update(PriceList item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            var priceList = restContext.PriceLists.Find(item.PriceId);
            if (priceList == null)
            {
                return false;
            }
            priceList.DishId = item.DishId;
            priceList.Portion = item.Portion;
            priceList.PricePerPortion = item.PricePerPortion;
            return true;
        }

    }
}
