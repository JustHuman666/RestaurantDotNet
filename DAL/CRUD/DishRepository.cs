﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Context;
using DAL.Enteties;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.CRUD
{
    public class DishRepository : IRepository<Dish>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly RestaurantContext restContext;


        /// <summary>
        /// Constructor of repository with given context
        /// </summary>
        /// <param name="context">context of DB</param>
        public DishRepository(RestaurantContext context)
        {
            restContext = context;
        }

        public bool Create(Dish item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            if (restContext.Dishes.Find(item.DishId) != null)
            {
                return false;
            }
            restContext.Dishes.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var dish = restContext.Dishes.Find(id);
            if (dish == null)
            {
                return false;
            }
            restContext.Dishes.Remove(dish);
            return true;
        }

        public IEnumerable<Dish> GetAll()
        {
            return restContext.Dishes;
        }

        public Dish SearchById(int id)
        {
            return restContext.Dishes.Include(x => x.Ingredients).ToList().Find(dish => dish.DishId == id);
        }

        public bool Update(Dish item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            var dish = restContext.Dishes.Find(item.DishId);
            if (dish == null)
            {
                return false;
            }
            dish.DishName = item.DishName;
            return true;
        }

    }
}
