﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Context;
using DAL.Enteties;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.CRUD
{
    public class OrderRepository : IRepository<Order>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly RestaurantContext restContext;

        /// <summary>
        /// Constructor of repository with given context
        /// </summary>
        /// <param name="context">context of DB</param>
        public OrderRepository(RestaurantContext context)
        {
            restContext = context;
        }

        public bool Create(Order item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            if (restContext.Orders.Find(item.OrderId) != null)
            {
                return false;
            }
            restContext.Orders.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var order = restContext.Orders.Find(id);
            if (order == null)
            {
                return false;
            }
            restContext.Orders.Remove(order);
            return true;
        }

        public IEnumerable<Order> GetAll()
        {
            return restContext.Orders;
        }

        public Order SearchById(int id)
        {
            return restContext.Orders.Include(x => x.DishesList).ToList().Find(ord => ord.OrderId == id);
        }

        public bool Update(Order item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            var order = restContext.Orders.Find(item.OrderId);
            if (order == null)
            {
                return false;
            }
            order.OrderDate = item.OrderDate;
            order.CustomerName = item.CustomerName;
            order.TotalPrice = item.TotalPrice;
            return true;
        }

    }
}
