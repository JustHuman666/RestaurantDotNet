﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Context;
using DAL.Enteties;
using DAL.Interfaces;

namespace DAL.CRUD
{
    public class OrderDishesRepository : IRepository<OrderDishes>
    {
        /// <summary>
        /// Context of DB
        /// </summary>
        private readonly RestaurantContext restContext;

        /// <summary>
        /// Constructor of repository with given context
        /// </summary>
        /// <param name="context">context of DB</param>
        public OrderDishesRepository(RestaurantContext context)
        {
            restContext = context;
        }

        public bool Create(OrderDishes item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            var orderDishes = restContext.OrderDishes.Find(item.PriceId);
            if (orderDishes != null)
            {
                return false;
            }
            restContext.OrderDishes.Add(item);
            return true;
        }

        public bool Delete(int id)
        {
            var ordDish = restContext.OrderDishes.Find(id);
            if (ordDish == null)
            {
                return false;
            }
            restContext.OrderDishes.Remove(ordDish);
            return true;
        }

        public IEnumerable<OrderDishes> GetAll()
        {
            return restContext.OrderDishes;
        }

        public OrderDishes SearchById(int id)
        {
            return restContext.OrderDishes.Find(id);
        }

        public bool Update(OrderDishes item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"{nameof(item)} null exception");
            }
            var orderDishes = restContext.OrderDishes.Find(item.OrderDishesId);
            if (orderDishes == null)
            {
                return false;
            }
            orderDishes.OrderId = item.OrderId;
            orderDishes.PriceId = item.PriceId;
            orderDishes.PortionCount = item.PortionCount;
            return true;
        }
    }
}
