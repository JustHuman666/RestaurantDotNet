﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Enteties
{
    public class Dish
    {
        /// <summary>
        /// An id of dish
        /// </summary>
        [Key]
        public int DishId { get; set; }

        /// <summary>
        /// The name of dish
        /// </summary>
        public string DishName { get; set; }

        /// <summary>
        /// List of ingredients for this dish
        /// </summary>
        public virtual ICollection<Ingredient> Ingredients { get; set; }

        /// <summary>
        /// List of prices for this dish
        /// </summary>
        public virtual ICollection<PriceList> Prices { get; set; }

    }
}
