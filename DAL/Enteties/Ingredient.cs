﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Enteties
{
    public class Ingredient
    {
        /// <summary>
        /// An id of ingredient
        /// </summary>
        public int IngredientId { get; set; }

        /// <summary>
        /// The name of ingredient
        /// </summary>
        public string IngredientName { get; set; }
        
        /// <summary>
        /// The list of dishes wich included this ingridient
        /// </summary>
        public virtual ICollection<Dish> Dishes { get; set; }
    }
}
