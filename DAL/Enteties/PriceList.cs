﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace DAL.Enteties
{
    public class PriceList
    {
        /// <summary>
        /// The id of price for dish
        /// </summary>
        [Key]
        public int PriceId { get; set; }

        /// <summary>
        /// The id of dish in order
        /// </summary>
        public int DishId { get; set; }

        /// <summary>
        /// The instance of dish with this price
        /// </summary>
        public Dish Dish { get; set; }
        
        /// <summary>
        /// The weight of portion with this price
        /// </summary>
        public int Portion { get; set; }

        /// <summary>
        /// Price for one such portion of this dish
        /// </summary>
        public decimal PricePerPortion { get; set; }

        /// <summary>
        /// The list of orders where are such dishes with this price
        /// </summary>
        public virtual ICollection<OrderDishes> OrdersList { get; set; }
    }
}
