﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Enteties
{
    public class Order
    {
        /// <summary>
        /// The id of order
        /// </summary>
        [Key]
        public int OrderId { get; set; }

        /// <summary>
        /// Total price of this order
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// List of dishes in this order
        /// </summary>
        public virtual ICollection<OrderDishes> DishesList { get; set; }

        /// <summary>
        /// Name of customer for this order
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Date of creating of order
        /// </summary>
        public DateTime OrderDate { get; set; }
    }
}
