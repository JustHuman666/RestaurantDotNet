﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Enteties
{
    public class OrderDishes
    {
        /// <summary>
        /// The id of dish in order
        /// </summary>
        [Key]
        public int OrderDishesId { get; set; }

        /// <summary>
        /// The id of order which has this dish in
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// The instance of order which has this dish in
        /// </summary>
        public Order order { get; set; }

        /// <summary>
        /// The id of price which has this dish
        /// </summary>
        public int PriceId { get; set; }
        /// <summary>
        /// The instance of price which has this dish
        /// </summary>
        [ForeignKey("PriceId")]
        public PriceList DishPrice { get; set; }

        /// <summary>
        /// The amount of portion of this dish in this order
        /// </summary>
        public int PortionCount { get; set; }
    }
}
