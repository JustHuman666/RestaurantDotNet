﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVC.Models
{
    public class OrderDishesView
    {
        /// <summary>
        /// The id of dish in order
        /// </summary>
        public int OrderDishesId { get; set; }

        /// <summary>
        /// The id of order which has this dish in
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// The id of price which has this dish
        /// </summary>
        public int PriceId { get; set; }

        /// <summary>
        /// The amount of portion of this dish in this order
        /// </summary>
        public int PortionCount { get; set; }
    }
}
