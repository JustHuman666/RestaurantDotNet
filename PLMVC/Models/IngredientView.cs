﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLMVC.Models
{
    public class IngredientView
    {
        /// <summary>
        /// An id of ingredient
        /// </summary>
        public int IngredientId { get; set; }

        /// <summary>
        /// The name of ingredient
        /// </summary>
        public string IngredientName { get; set; }
    }
}
