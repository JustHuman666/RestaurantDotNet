﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.EntetiesDTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PLMVC.Infrastructure;
using PLMVC.Models;

namespace PLMVC.Controllers
{
    public class DishController : Controller
    {
        private readonly IDishService dishService;
        private readonly IPriceListService priceService;

        public DishController()
        {
            dishService = ConfClass.GetService(typeof(IDishService)) as IDishService;
            priceService = ConfClass.GetService(typeof(IPriceListService)) as IPriceListService;
        }
        // GET: Dish
        public ActionResult Index()
        {
            return View();
        }

        // GET all diches
        public ActionResult GetAll()
        {
            try
            {
                var dishes = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                }).CreateMapper();
                var dishView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishes);
                ViewBag.Dishes = dishView;
                var prices = priceService.GetPrices();
                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(prices);
                ViewBag.Prices = pricesView;
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return View();
        }

        // GET: Ingredient/Add
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(DishView dish)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => 
                {
                    cfg.CreateMap<DishView, DishDto>();
                }).CreateMapper();
                var dishDto = mapper.Map<DishView, DishDto>(dish);
                dishService.Add(dishDto);
                return RedirectToAction(nameof(GetAll));
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        public ActionResult AddPrice(int id)
        {
            ViewData["DishId"] = id;
            return View();
        }

        [HttpPost]
        public ActionResult AddPrice(PriceListView priceView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PriceListView, PriceListDto>();
                }).CreateMapper();
                var price = mapper.Map<PriceListView, PriceListDto>(priceView);
                priceService.Add(price);
                return RedirectToAction(nameof(GetAll));
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult Edit(DishView dishView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishView, DishDto>()).CreateMapper();
                var dish = mapper.Map<DishView, DishDto>(dishView);
                dishService.Update(dish);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult Delete(int id)
        {
            try
            {
                dishService.Delete(id);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult Find()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Find(DishView dishView)
        {
            try
            {
                var dishDto = dishService.FindByName(dishView.DishName);
                var mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                }).CreateMapper();
                var dish = mapper.Map<DishDto, DishView>(dishDto);
                ViewBag.Dishes = new List<DishView>() { dish };
                var prices = priceService.GetPrices();
                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(prices);
                ViewBag.Prices = pricesView;
                return View("GetAll");
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }
        public ActionResult EditPrice(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceListDto, PriceListView > ()).CreateMapper();
            var price = mapper.Map<PriceListDto, PriceListView > (priceService.GetPrices().FirstOrDefault(pr => pr.PriceId == id));
            ViewBag.Price = price;
            return View();
        }

        [HttpPost]
        public ActionResult EditPrice(PriceListView priceList)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceListView, PriceListDto>()).CreateMapper();
                var price = mapper.Map<PriceListView, PriceListDto>(priceList);
                priceService.Update(price);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult DeletePrice(int id)
        {
            try
            {
                priceService.Delete(id);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult GetIngredients(int id)
        {
            try
            {
                var dishDto = dishService.GetDishes().FirstOrDefault(dish => dish.DishId == id);
                var ingredientsDto = dishService.GetIngredientsByDishId(id);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<IngredientDto, IngredientView>();
                }).CreateMapper();
                var dishView = mapper.Map<DishDto, DishView>(dishDto);
                var ingredients = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredientsDto);
                ViewBag.Ingredients = ingredients;
                ViewBag.Dish = dishView;
                return View();
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                ViewData["DishId"] = id;
                return View("ErrorIngredient");
            }
        }
        public ActionResult AddIngredient(int id)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                var dishDto = dishService.GetDishes().FirstOrDefault(d => d.DishId == id);
                ViewBag.Dish = mapper.Map<DishDto, DishView>(dishDto);
                return View();
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddIngredient(int dishID, int IngredientId)
        {
            try
            {
                dishService.AddIngredient(dishID, IngredientId);
                return RedirectToAction(nameof(GetAll));
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                ViewData["DishId"] = dishID;
                return View("ErrorIngredient");
            }
        }

        public ActionResult DeleteIngredient(int dishId, int ingrId)
        {
            try
            {
                dishService.DeleteIngredient(dishId, ingrId);
                
                return RedirectToAction(nameof(GetAll));
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            
        }

    }
}