﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.EntetiesDTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PLMVC.Infrastructure;
using PLMVC.Models;

namespace PLMVC.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;
        private readonly IOrderDishesService orderDishService;
        private readonly IPriceListService priceService;
        private readonly IDishService dishService;

        public OrderController()
        {
            orderService = ConfClass.GetService(typeof(IOrderService)) as IOrderService;
            orderDishService = ConfClass.GetService(typeof(IOrderDishesService)) as IOrderDishesService;
            priceService = ConfClass.GetService(typeof(IPriceListService)) as IPriceListService;
            dishService = ConfClass.GetService(typeof(IDishService)) as IDishService;
        }
        // GET: Dish
        public ActionResult Index()
        {
            return View();
        }

        // GET all diches
        public ActionResult GetAll()
        {
            try
            {
                var ordersDto = orderService.GetOrders();
                var mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                }).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                ViewBag.Orders = ordersView;
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return View();
        }

        // GET: Ingredient/Add
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(OrderView orderView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderView, OrderDto>();
                }).CreateMapper();
                orderView.OrderDate = DateTime.Now;
                var orderDto = mapper.Map<OrderView, OrderDto>(orderView);
                orderService.Add(orderDto);
                return RedirectToAction(nameof(GetAll));
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDto, OrderView>()).CreateMapper();
                var orderDto = orderService.GetOrders().FirstOrDefault(ord => ord.OrderId == id);
                var orderView = mapper.Map<OrderDto, OrderView>(orderDto);
                ViewBag.Order = orderView;
                return View();
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Edit(OrderView orderView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderView, OrderDto>()).CreateMapper();
                var orderDto = mapper.Map<OrderView, OrderDto>(orderView);
                orderService.Update(orderDto);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult Delete(int id)
        {
            try
            {
                orderService.Delete(id);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult Find()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Find(OrderView orderView)
        {
            try
            {
                var ordersDto = orderService.FindOrdersByCustomer(orderView.CustomerName);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                }).CreateMapper();
                var dish = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                ViewBag.Orders = dish;
                var dishes = orderDishService.GetOrdersDishes();
                var dishesView = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(dishes);
                ViewBag.Dishes = dishesView;
                return View("GetAll");
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        public ActionResult EditDish(int id)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesDto, OrderDishesView>()).CreateMapper();
                var orderDishView = mapper.Map<OrderDishesDto, OrderDishesView>(orderDishService
                    .GetOrdersDishes().FirstOrDefault(ord => ord.OrderDishesId == id));
                ViewBag.OrderDish = orderDishView;
                return View();
            }
            catch(Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            
        }

        [HttpPost]
        public ActionResult EditDish(OrderDishesView orderDishView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesView, OrderDishesDto>()).CreateMapper();
                var orderDishDto = mapper.Map<OrderDishesView, OrderDishesDto>(orderDishView);
                orderDishService.Update(orderDishDto);
                UpdateTotalPrice(orderDishView.OrderId);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }


        public ActionResult GetDishes(int id)
        {
            try
            {
                var orderDto = orderService.GetOrders().FirstOrDefault(ord => ord.OrderId == id);
                var dishesDto = orderDishService.FindOrderDishesByOrderId(id);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                var orderView = mapper.Map<OrderDto, OrderView>(orderDto);
                var dishes = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(dishesDto);
                var prices = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(priceService.GetPrices());
                List<PriceListView> finalPrices = new List<PriceListView>();
                foreach (var dish in dishes)
                {
                    var pricesForDish = prices.Where(pr => pr.PriceId == dish.PriceId);
                    foreach (var price in pricesForDish)
                    {
                        finalPrices.Add(price);
                    }
                }
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishService.GetDishes());
                List<DishView> finalDishes = new List<DishView>();
                foreach (var price in finalPrices)
                {
                    var dishForPrice = dishesView.Where(dish => dish.DishId == price.DishId);
                    foreach (var dish in dishForPrice)
                    {
                        finalDishes.Add(dish);
                    }
                }
                ViewBag.OrdDishes = dishes;
                ViewBag.Order = orderView;
                ViewBag.Prices = finalPrices;
                ViewBag.Dishes = finalDishes;
                return View();
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                ViewData["OrderId"] = id;
                return View("ErrorOrder");
            }
        }
        public ActionResult AddDish(int id)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                }).CreateMapper();
                var orderDto = orderService.GetOrders().FirstOrDefault(d => d.OrderId == id);
                ViewBag.Order = mapper.Map<OrderDto, OrderView>(orderDto);
                return View();
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddDish(int OrderId, int priceId, int amount)
        {
            try
            {
                orderService.AddDishToOrder(OrderId, priceId, amount);
                UpdateTotalPrice(OrderId);
                return RedirectToAction(nameof(GetAll));
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        public ActionResult DeleteDishById(int id)
        {
            try
            {
                var orderDtoId = orderDishService.GetOrdersDishes().FirstOrDefault(ord => ord.OrderDishesId == id).OrderId;
                orderDishService.Delete(id);
                UpdateTotalPrice(orderDtoId);
                return RedirectToAction(nameof(GetAll));
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }

        }

        private void UpdateTotalPrice(int id)
        {
            var orderDto = orderService.GetOrders().FirstOrDefault(ord => ord.OrderId == id);
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<OrderDto, OrderView>();
                cfg.CreateMap<OrderView, OrderDto>();
            }).CreateMapper();
            var orderView = mapper.Map<OrderDto, OrderView>(orderDto);
            var orderDishesDto = orderDishService.GetOrdersDishes().Where(ord => ord.OrderId == orderView.OrderId);
            decimal totalPrice = 0;
            foreach (var item in orderDishesDto)
            {
                var price = priceService.GetPrices().FirstOrDefault(pr => pr.PriceId == item.PriceId);
                totalPrice += item.PortionCount * price.PricePerPortion;
            }
            orderView.TotalPrice = totalPrice;
            orderService.Update(mapper.Map<OrderView, OrderDto>(orderView));
        }
    }
}