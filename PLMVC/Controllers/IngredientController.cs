﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.EntetiesDTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PLMVC.Infrastructure;
using PLMVC.Models;

namespace PLMVC.Controllers
{
    public class IngredientController : Controller
    {
        private readonly IIngredientService ingredientService;
        public IngredientController()
        {
            ingredientService = ConfClass.GetService(typeof(IIngredientService)) as IIngredientService;
        }
        // main page
        public ActionResult Index()
        {
            return View();
        }

        // GET all ingredients
        public ActionResult GetAll()
        {
            try
            {
                var ingredients = ingredientService.GetIngredients();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, IngredientView>()).CreateMapper();
                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredients);
                ViewBag.Ingredients = ingredientsView;
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return View();
        }

        // GET: Ingredient/Add
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(IngredientView ingredientView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientView, IngredientDto>()).CreateMapper();
                var ingredient = mapper.Map<IngredientView, IngredientDto>(ingredientView);
                ingredientService.Add(ingredient);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult Edit(int id)
        {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult Edit(IngredientView ingredientView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientView, IngredientDto>()).CreateMapper();
                var ingredient = mapper.Map<IngredientView, IngredientDto>(ingredientView);
                ingredientService.Update(ingredient);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }
        
        public ActionResult Delete(int id)
        {
            try
            {
                ingredientService.Delete(id);
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
            return RedirectToAction(nameof(GetAll));
        }

        public ActionResult Find()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Find(IngredientView ingred)
        {
            try
            {
                var ingredient = ingredientService.FindByName(ingred.IngredientName);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, IngredientView>()).CreateMapper();
                var ingredientView = mapper.Map<IngredientDto, IngredientView>(ingredient);
                ViewBag.Ingredients = new List<IngredientView>() { ingredientView };
                return View("GetAll");
            }
            catch (Exception exception)
            {
                ViewData["Message"] = exception.Message;
                return View("Error");
            }
        }

        
    }
}