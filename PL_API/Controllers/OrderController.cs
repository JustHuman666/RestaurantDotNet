﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.EntetiesDTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL_API.Models;
using PL_API.Infrastructure;
using Newtonsoft.Json;

namespace PL_API.Controllers
{
    /// <summary>
    /// Controller for working with orders
    /// </summary>
    [ApiController]
    [Route("restaurant/[controller]")]
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;
        private readonly IOrderDishesService orderDishService;
        private readonly IPriceListService priceService;
        private readonly IDishService dishService;

        public OrderController()
        {
            orderService = ConfClass.GetService(typeof(IOrderService)) as IOrderService;
            orderDishService = ConfClass.GetService(typeof(IOrderDishesService)) as IOrderDishesService;
            priceService = ConfClass.GetService(typeof(IPriceListService)) as IPriceListService;
            dishService = ConfClass.GetService(typeof(IDishService)) as IDishService;
        }

        /// <summary>
        /// Request for getting all of the orders from DB
        /// </summary>
        /// <returns>Status code and the list of all orders</returns>
        [HttpGet]
        [Route("getall")]
        public ActionResult GetAll()
        {
            try
            {
                var ordersDto = orderService.GetOrders();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                }).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                return Ok(ordersView);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for creating a new instance of order
        /// </summary>
        /// <param name="orderView">The instance of new order that should be created</param>
        /// <returns>Status code and instance of created order</returns>
        [HttpPost]
        [Route("add")]
        public ActionResult Add([FromBody]OrderView orderView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderView, OrderDto>();
                    cfg.CreateMap<OrderDto, OrderView>();
                }).CreateMapper();
                orderView.OrderDate = DateTime.Now;
                var orderDto = mapper.Map<OrderView, OrderDto>(orderView);
                orderService.Add(orderDto);
                var order = orderService.GetOrders().FirstOrDefault(ord => ord.OrderDate == orderView.OrderDate);
                return Ok(mapper.Map<OrderDto, OrderView>(order));
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for updating some chosen order info
        /// </summary>
        /// <param name="id">The id of order that should be updated</param>
        /// <param name="newOrderView">The instance of order with new info</param>
        /// <returns>Status code and instance of updated order</returns>
        [HttpPut]
        [Route("edit/{id}")]
        public ActionResult Edit(int id, [FromBody]OrderView newOrderView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => 
                { 
                    cfg.CreateMap<OrderDto, OrderView>();
                }).CreateMapper();
                var orderDto = orderService.GetOrders().FirstOrDefault(ord => ord.OrderId == id);
                orderDto.CustomerName = newOrderView.CustomerName;
                orderService.Update(orderDto);
                var orderView = mapper.Map<OrderDto, OrderView>(orderService.GetOrders().FirstOrDefault(ord => ord.OrderId == id));
                return Ok(orderView);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for deleting of chosen order from DB
        /// </summary>
        /// <param name="id">The id of order that should be deleted</param>
        /// <returns>Status code and message of result</returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                orderService.Delete(id);
                return Ok("This order was successfully deleted.");
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for adding of chosen dish from menu to chosen order
        /// </summary>
        /// <param name="orderId">The id of order which chosen dish should be added to</param>
        /// <param name="orderDishesView">The instance of dish for this order that should be created and added</param>
        /// <returns>Status code and instance of created dish for this order</returns>
        [HttpPost]
        [Route("adddish/{orderId}")]
        public ActionResult AddDish(int orderId, [FromBody]OrderDishesView orderDishesView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                }).CreateMapper();
                orderDishesView.OrderId = orderId;
                orderService.AddDishToOrder(orderId, orderDishesView.PriceId, orderDishesView.PortionCount);
                UpdateTotalPrice(orderId);
                var orderDish = mapper.Map<OrderDishesDto, OrderDishesView>(orderDishService.GetOrdersDishes()
                    .FirstOrDefault(ord => ord.OrderId == orderDishesView.OrderId && ord.PriceId == orderDishesView.PriceId && ord.PortionCount == orderDishesView.PortionCount));
                return Ok(orderDish);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for updating some info of chosen dish in order
        /// </summary>
        /// <param name="id">The id of dish in order that should be updated</param>
        /// <param name="orderDishView">The instance with new dish info for the order</param>
        /// <returns>Status code and instance of updated dish in order</returns>
        [HttpPut]
        [Route("editdish/{id}")]
        public ActionResult EditDish(int id, [FromBody]OrderDishesView orderDishView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => 
                {
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                }).CreateMapper();
                var orderDishDto = orderDishService.GetOrdersDishes().FirstOrDefault(ord => ord.OrderDishesId == id);
                orderDishDto.PortionCount = orderDishView.PortionCount;
                orderDishService.Update(orderDishDto);
                UpdateTotalPrice(orderDishDto.OrderId);
                var orderDish = mapper.Map<OrderDishesDto, OrderDishesView>(orderDishService
                    .GetOrdersDishes().FirstOrDefault(ord => ord.OrderDishesId == id));
                return Ok(orderDish);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for deleting of chosen dish from the order
        /// </summary>
        /// <param name="id">The id of dish in order that should be deleted</param>
        /// <returns>Status code and message of result</returns>
        [HttpDelete]
        [Route("deletedish/{id}")]
        public ActionResult DeleteDishById(int id)
        {
            try
            {
                var orderDtoId = orderDishService.GetOrdersDishes().FirstOrDefault(ord => ord.OrderDishesId == id).OrderId;
                orderDishService.Delete(id);
                UpdateTotalPrice(orderDtoId);
                return Ok("This dish was successfully deleted from this order.");
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }

        }

        /// <summary>
        /// Request for getting the list of all dishes in chosen order
        /// </summary>
        /// <param name="id">The id of order which dishes should be found of</param>
        /// <returns>Status code and the list of all dishes in this order</returns>
        [HttpGet]
        [Route("getdishes/{id}")]
        public ActionResult GetDishes(int id)
        {
            try
            {
                var orderDto = orderService.GetOrders().FirstOrDefault(ord => ord.OrderId == id);
                var dishesDto = orderDishService.FindOrderDishesByOrderId(id);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                Dictionary<int, Dictionary<string, Dictionary<string, int>>> orderDishes = new Dictionary<int, Dictionary<string, Dictionary<string, int>>>();
                var dishesView = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(dishesDto);
                foreach (var orderDish in dishesDto)
                {
                    var price = priceService.GetPrices().FirstOrDefault(pr => pr.PriceId == orderDish.PriceId);
                    var dish = dishService.GetDishes().FirstOrDefault(dish => dish.DishId == price.DishId);
                    Dictionary<string, Dictionary<string, int>> prices = new Dictionary<string, Dictionary<string, int>>();
                    var priceView = mapper.Map<PriceListDto, PriceListView>(price);
                    var dishView = mapper.Map<DishDto, DishView>(dish);
                    Dictionary<string, int> portions = new Dictionary<string, int>();
                    portions.Add("weight", priceView.Portion);
                    portions.Add("price", (int)priceView.PricePerPortion);
                    portions.Add("amount", orderDish.PortionCount);
                    prices.Add(dish.DishName, portions);
                    orderDishes.Add(orderDish.OrderDishesId, prices);
                }
                return Ok(JsonConvert.SerializeObject(orderDishes, Formatting.Indented));
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for finding of some orders of chosen customer by his/her name 
        /// </summary>
        /// <param name="name">The name of customer whose order should be found of</param>
        /// <returns>Status code and the list of all orders of this customer</returns>
        [HttpGet]
        [Route("find/{name}")]
        public ActionResult Find(string name)
        {
            try
            {
                var ordersDto = orderService.FindOrdersByCustomer(name);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                }).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                return Ok(ordersView);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for updating of total price of chosen order
        /// </summary>
        /// <param name="id">The id of order which total price should be updated of</param>
        [HttpPut]
        [Route("updateprice/{id}")]
        public ActionResult UpdateTotalPrice(int id)
        {
            try
            {
                var orderDto = orderService.GetOrders().FirstOrDefault(ord => ord.OrderId == id);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderView, OrderDto>();
                }).CreateMapper();
                var orderView = mapper.Map<OrderDto, OrderView>(orderDto);
                var orderDishesDto = orderDishService.GetOrdersDishes().Where(ord => ord.OrderId == orderView.OrderId);
                decimal totalPrice = 0;
                foreach (var item in orderDishesDto)
                {
                    var price = priceService.GetPrices().FirstOrDefault(pr => pr.PriceId == item.PriceId);
                    totalPrice += item.PortionCount * price.PricePerPortion;
                }
                orderView.TotalPrice = totalPrice;
                orderService.Update(mapper.Map<OrderView, OrderDto>(orderView));
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }
    }
}