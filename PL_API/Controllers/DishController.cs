﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.EntetiesDTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL_API.Models;
using PL_API.Infrastructure;
using Newtonsoft.Json;

namespace PL_API.Controllers
{
    /// <summary>
    /// Controller for working with dishes
    /// </summary>
    [ApiController]
    [Route("restaurant/[controller]")]
    public class DishController : Controller
    {
        private readonly IDishService dishService;
        private readonly IPriceListService priceService;

        public DishController()
        {
            dishService = ConfClass.GetService(typeof(IDishService)) as IDishService;
            priceService = ConfClass.GetService(typeof(IPriceListService)) as IPriceListService;
        }


        /// <summary>
        /// Request for getting menu from DB
        /// </summary>
        /// <returns>Status code of result and list of dishes with their prices</returns>
        [HttpGet]
        [Route("getall")]
        public ActionResult GetAll()
        {
            try
            {
                var dishes = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                }).CreateMapper();
                var dishView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishes);
                var prices = priceService.GetPrices();
                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(prices);
                Dictionary<string, List<PriceListView>> menu = new Dictionary<string, List<PriceListView>>();
                foreach (var dish in dishView)
                {
                    var priceList = pricesView.Where(pr => pr.DishId == dish.DishId).ToList();
                    menu.Add(dish.DishName, priceList);
                }
                return Ok(JsonConvert.SerializeObject(menu, Formatting.Indented));
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for creating a new instance of a dish
        /// </summary>
        /// <param name="dish">The instance of new dish</param>
        /// <returns>Status code and instance of created dish</returns>
        [HttpPost]
        [Route("add")]
        public ActionResult Add([FromBody]DishView dish)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishView, DishDto>();
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                var dishDto = mapper.Map<DishView, DishDto>(dish);
                dishService.Add(dishDto);
                var dishView = mapper.Map<DishDto, DishView>(dishService.FindByName(dish.DishName));
                return Ok(dishView);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for updating some info of chosen dish
        /// </summary>
        /// <param name="id">The id of chosen dish for updating</param>
        /// <param name="dishView">The instance of dish with new info</param>
        /// <returns>Status code and instance of updated dish</returns>
        [HttpPut]
        [Route("edit/{id}")]
        public ActionResult Edit(int id, [FromBody]DishView dishView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishView, DishDto>()).CreateMapper();
                dishView.DishId = id;
                var dish = mapper.Map<DishView, DishDto>(dishView);
                dishService.Update(dish);
                return Ok(dishView);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for deleting of chosen dish from DB
        /// </summary>
        /// <param name="id">The id of chosen dish</param>
        /// <returns>Status code and message with result</returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                dishService.Delete(id);
                return Ok("This dish was successfully deleted.");
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for adding new portion type with neede price for chosen dish
        /// </summary>
        /// <param name="id">The id of dish where new portion type should be added for</param>
        /// <param name="priceView">The instance of new price and new portion</param>
        /// <returns>Status code and instance of created portion info</returns>
        [HttpPost]
        [Route("addprice/{id}")]
        public ActionResult AddPrice(int id, [FromBody]PriceListView priceView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PriceListView, PriceListDto>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                }).CreateMapper();
                priceView.DishId = id;
                var price = mapper.Map<PriceListView, PriceListDto>(priceView);
                priceService.Add(price);
                return Ok(mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(priceService.GetPrices().Where(pr => pr.DishId == priceView.DishId)));
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for updating some info about chosen portion and price
        /// </summary>
        /// <param name="id">The id of chosen price info</param>
        /// <param name="priceList">The instance with new info for chosen price</param>
        /// <returns>Status code and instance of updated price</returns>
        [HttpPut]
        [Route("editprice/{id}")]
        public ActionResult EditPrice(int id, [FromBody]PriceListView priceList)
        {
            try
            {
                var priceDto = priceService.GetPrices().FirstOrDefault(pr => pr.PriceId == id);
                priceDto.Portion = priceList.Portion;
                priceDto.PricePerPortion = priceList.PricePerPortion;
                priceService.Update(priceDto);
                return Ok(priceList);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for deleting of chosen portion type of the dish
        /// </summary>
        /// <param name="id">The id of chosen portion type that should be deleted</param>
        /// <returns>Status code and message of result</returns>
        [HttpDelete]
        [Route("deleteprice/{id}")]
        public ActionResult DeletePrice(int id)
        {
            try
            {
                priceService.Delete(id);
                return Ok("This price for dish was successfully deleted.");
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for getting the list of all ingredients that chosen dish consist of
        /// </summary>
        /// <param name="id">The id of chosen dish which ingredients are getting</param>
        /// <returns>Status code and the list of all ingredients of this dish</returns>
        [HttpGet]
        [Route("getingredients/{id}")]
        public ActionResult GetIngredients(int id)
        {
            try
            {
                var dishDto = dishService.GetDishes().FirstOrDefault(dish => dish.DishId == id);
                var ingredientsDto = dishService.GetIngredientsByDishId(id);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<IngredientDto, IngredientView>();
                }).CreateMapper();
                var dishView = mapper.Map<DishDto, DishView>(dishDto);
                var ingredients = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredientsDto);
                return Ok(ingredients);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for adding of chosen ingredient for chosen dish
        /// </summary>
        /// <param name="dishId">The id of dish where ingredient should be added for</param>
        /// <param name="ingrId">The id of ingredient that should be added for chosen dish</param>
        /// <returns>Status code and the list of all ingredients of this dish</returns>
        [HttpPost]
        [Route("addingredient/{dishId}/{ingrId}")]
        public ActionResult AddIngredient(int dishId, int ingrId)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<IngredientDto, IngredientView>();
                }).CreateMapper();
                var dishDto = dishService.GetDishes().FirstOrDefault(d => d.DishId == dishId);
                dishService.AddIngredient(dishId, ingrId);
                var ingredientsDto = dishService.GetIngredientsByDishId(dishId);
                var ingredients = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredientsDto);
                return Ok(ingredients);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for deleting of chosen ingredient from chosen dish
        /// </summary>
        /// <param name="dishId">The id of chosen dish for deleting ingredient from</param>
        /// <param name="ingrId">The id of chosen ingredint for deleting of</param>
        /// <returns>Status code and message of result</returns>
        [HttpDelete]
        [Route("deleteingredient/{dishId}/{ingrId}")]
        public ActionResult DeleteIngredient(int dishId, int ingrId)
        {
            try
            {
                dishService.DeleteIngredient(dishId, ingrId);
                return Ok("This ingredient was successfully deleted for this dish");
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for finding of instance and additional info of dish with chosen name
        /// </summary>
        /// <param name="name">The name of dish that is finding</param>
        /// <returns></returns>
        [HttpGet]
        [Route("find/{name}")]
        public ActionResult Find(string name)
        {
            try
            {
                var dishDto = dishService.FindByName(name);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                }).CreateMapper();
                var dish = mapper.Map<DishDto, DishView>(dishDto);
                var prices = priceService.GetPrices().Where(pr => pr.DishId == dish.DishId);
                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(prices).ToList();
                Dictionary<string, List<PriceListView>> dishInfo = new Dictionary<string, List<PriceListView>>();
                dishInfo.Add(dish.DishName, pricesView);
                return Ok(JsonConvert.SerializeObject(dishInfo, Formatting.Indented));
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }
        /// <summary>
        /// Request for finding of instance of dish with chosen name
        /// </summary>
        /// <param name="name">The name of dish that is finding</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getbyname/{name}")]
        public ActionResult GetByName(string name)
        {
            try
            {
                var dishDto = dishService.FindByName(name);
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                var dish = mapper.Map<DishDto, DishView>(dishDto);
                return Ok(dish);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

    }
}