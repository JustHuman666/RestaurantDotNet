﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.EntetiesDTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL_API.Infrastructure;
using PL_API.Models;

namespace PL_API.Controllers
{
    /// <summary>
    /// Controller for working with ingredients
    /// </summary>
    [ApiController]
    [Route("restaurant/[controller]")]
    public class IngredientController : ControllerBase
    {
        private readonly IIngredientService ingredientService;
        public IngredientController()
        {
            ingredientService = ConfClass.GetService(typeof(IIngredientService)) as IIngredientService;
        }

        /// <summary>
        /// Request for getting all possible ingredients from DB
        /// </summary>
        /// <returns>Status code of result and list of ingredients</returns>
        [HttpGet]
        [Route("getall")]
        public ActionResult GetAll()
        {
            try
            {
                var ingredients = ingredientService.GetIngredients();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, IngredientView>()).CreateMapper();
                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredients);
                return Ok(ingredientsView);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for creating a new instance of ingredient
        /// </summary>
        /// <param name="ingredientView">The name of new ingredient</param>
        /// <returns>Status code and instance of created ingredient</returns>
        [HttpPost]
        [Route("add")]
        public ActionResult Add([FromBody]IngredientView ingredientView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                { cfg.CreateMap<IngredientView, IngredientDto>();
                    cfg.CreateMap<IngredientDto, IngredientView>();
                }).CreateMapper();
                var ingredient = mapper.Map<IngredientView, IngredientDto>(ingredientView);
                ingredientService.Add(ingredient);
                var createdDto = ingredientService.GetIngredients().FirstOrDefault(ingr => ingr.IngredientName == ingredientView.IngredientName);
                return Ok(mapper.Map<IngredientDto, IngredientView>(createdDto));
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for updating of ingredient info
        /// </summary>
        /// <param name="id">The id of needed ingredient</param>
        /// <param name="ingredientView">Instance of ingredient with new info</param>
        /// <returns>Status code and instance of updated ingredient</returns>
        [HttpPut]
        [Route("edit/{id}")]
        public ActionResult Edit(int id, [FromBody]IngredientView ingredientView)
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientView, IngredientDto>()).CreateMapper();
                ingredientView.IngredientId = id;
                var ingredient = mapper.Map<IngredientView, IngredientDto>(ingredientView);
                ingredientService.Update(ingredient);
                return Ok(ingredient);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for deleting of chosen ingredient from DB
        /// </summary>
        /// <param name="id">The id of ingredient that should be deleted</param>
        /// <returns>Status code and message with result</returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                ingredientService.Delete(id);
                return Ok("This ingredient was successfully deleted.");
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }

        /// <summary>
        /// Request for finding of instance of ingredient by its name
        /// </summary>
        /// <param name="name">The name of ingredient that is finding</param>
        /// <returns>Status code and instance of finded ingredient</returns>
        [HttpGet]
        [Route("find/{name}")]
        public ActionResult Find(string name)
        {
            try
            {
                var ingredient = ingredientService.FindByName(name);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, IngredientView>()).CreateMapper();
                var ingredientView = mapper.Map<IngredientDto, IngredientView>(ingredient);
                return Ok(ingredientView);
            }
            catch (Exception exception)
            {
                return BadRequest(new ErrorView() { Message = exception.Message });
            }
        }


    }
}