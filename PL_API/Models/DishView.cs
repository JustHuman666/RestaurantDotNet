﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PL_API.Models
{
    public class DishView
    {
        /// <summary>
        /// An id of dish
        /// </summary>
        public int DishId { get; set; }

        /// <summary>
        /// The name of dish
        /// </summary>
        public string DishName { get; set; }
    }
}
