﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PL_API.Models
{
    public class IngredientView
    {
        /// <summary>
        /// An id of ingredient
        /// </summary>
        public int IngredientId { get; set; }

        /// <summary>
        /// The name of ingredient
        /// </summary>
        public string IngredientName { get; set; }
    }
}
