﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PL_API.Models
{
    public class ErrorView
    {
        public string Message { get; set; }
    }
}
