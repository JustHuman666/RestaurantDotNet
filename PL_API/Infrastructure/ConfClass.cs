﻿using System;
using BLL.Infrastructure;
using Ninject.Modules;
using Ninject;

namespace PL_API.Infrastructure
{
    /// <summary>
    /// Static class for configuration of program
    /// </summary>
    public static class ConfClass
    {
        /// <summary>
        /// Connection string of DB
        /// </summary>
        private static string ConectionString;

        /// <summary>
        /// The container of services
        /// </summary>
        private static NinjectModule Service;
        /// <summary>
        /// The container of unit of work
        /// </summary>
        private static NinjectModule UoW;

        /// <summary>
        /// Create standart kernel for containers
        /// </summary>
        private static StandardKernel Kernel;

        /// <summary>
        /// To get the service of given type from container
        /// </summary>
        /// <param name="serviceType">type of interface of service</param>
        /// <returns>the instance of service of given type</returns>
        public static object GetService(Type serviceType)
        {
            return Kernel.TryGet(serviceType);
        }

        public static void SetConString(string connection)
        {
            ConectionString = connection;
            Service = new IOCUoWModule(ConectionString);
            UoW = new IOCServiceModule();
            Kernel = new StandardKernel(Service, UoW);
        }
    }
}
