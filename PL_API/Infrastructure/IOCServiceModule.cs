﻿using Ninject.Modules;
using BLL.Interfaces;
using BLL.Services;


namespace PL_API.Infrastructure
{
    /// <summary>
    /// Class for creating container for services
    /// </summary>
    public class IOCServiceModule : NinjectModule
    {
        /// <summary>
        /// To load needed classes in container
        /// </summary>
        public override void Load()
        {
            Bind<IDishService>().To<DishService>();
            Bind<IIngredientService>().To<IngredientService>();
            Bind<IOrderService>().To<OrderService>();
            Bind<IPriceListService>().To<PriceListService>();
            Bind<IOrderDishesService>().To<OrderDishesService>();
        }
    }
}
