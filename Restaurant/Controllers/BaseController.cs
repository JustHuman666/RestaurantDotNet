﻿using System;
using System.Collections.Generic;
using System.Text;
using Restaurant.Interfaces;
using Restaurant.Controllers;


namespace Restaurant.Controllers
{
    public class BaseController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        public BaseController()
        {
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GoToIngredientController,
                [2] = GoToDishController,
                [3] = GoToOrderController,
                [4] = GoToPriceListController,
                [5] = GoToDisgesInOrdersController
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - work with ingredients" +
                "\n2 - work with dishes" +
                "\n3 - work with orders" +
                "\n4 - work with prices" +
                "\n5 - work with dishes in orders");
        }
        
        public IController GoToIngredientController()
        {
            return new IngredientController();
        }

        public IController GoToDishController()
        {
            return new DishController();
        }

        public IController GoToOrderController()
        {
            return new OrderController();
        }

        public IController GoToPriceListController()
        {
            return new PriceListController();
        }

        public IController GoToDisgesInOrdersController()
        {
            return new OrderDishesController();
        }

    }
}
