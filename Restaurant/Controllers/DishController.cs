﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Interfaces;
using Restaurant.Interfaces;
using Restaurant.Infrastructure;
using Restaurant.EntetiesView;
using BLL.EntetiesDTO;
using AutoMapper;

namespace Restaurant.Controllers
{
    public class DishController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        private readonly IDishService dishService;

        private readonly IIngredientService ingredientService;

        private readonly PriceListController priceListController;

        public DishController()
        {
            dishService = Configuration.GetService(typeof(IDishService)) as IDishService;
            ingredientService = Configuration.GetService(typeof(IIngredientService)) as IIngredientService;
            priceListController = new PriceListController();
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetDishesList,
                [2] = AddNewDish,
                [3] = ChangeDishInfo,
                [4] = FindDish,
                [5] = DeleteDish,
                [6] = AddIngredientForDish,
                [7] = DeleteIngredientFromDish,
                [8] = GetListOfIngredients,
                [9] = GetPricesForDish,
                [10] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - get the list of all dishes" +
                "\n2 - add new dish in database" +
                "\n3 - change information about dish" +
                "\n4 - find dish" +
                "\n5 - delete dish from database" +
                "\n6 - add ingredient for dish" +
                "\n7 - delete ingredient from dish" +
                "\n8 - get the list of all ingredients for one dish" +
                "\n9 - get prices for one dish" +
                "\n10 - return to the main menu\n");
        }

        public IController GetDishesList()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishDto, DishView>()).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes from database: \n");
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddNewDish()
        {
            try
            {
                Console.Write("Write the name of dish you want to add:   ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                DishView dishView = new DishView() { DishName = name };
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishView, DishDto>()).CreateMapper();
                var dishDto = mapper.Map<DishView, DishDto>(dishView);
                dishService.Add(dishDto);
                Console.WriteLine("Dish was added\n");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController ChangeDishInfo()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<DishView, DishDto>();
                }).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes from database: \n");
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose the number of dish you want to change information about: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > dishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var dishView = dishesView.ElementAt(number - 1);
                Console.Write("Write new name of dish or '0' if you don't want to change it:   ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                if (name != "0")
                {
                    dishView.DishName = name;
                }
                var dishDto = mapper.Map<DishView, DishDto>(dishView);
                dishService.Update(dishDto);
                Console.WriteLine("Changing information was successfully done!");

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController FindDish()
        {
            try
            {
                Console.Write("Write the name of dish you want to find:   ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                var dishDto = dishService.FindByName(name);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishDto, DishView>()).CreateMapper();
                var dishView = mapper.Map<DishDto, DishView>(dishDto);
                Console.WriteLine($"Dish id: {dishView.DishId}; Dish name: {dishView.DishName}\n");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteDish()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishDto, DishView>()).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes from database: \n");
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose the number of dish you want to delete: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > dishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var dishView = dishesView.ElementAt(number - 1);
                dishService.Delete(dishView.DishId);
                Console.WriteLine("Deleting of this dish was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddIngredientForDish()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<DishView, DishDto>();
                    cfg.CreateMap<IngredientDto, IngredientView>();
                }).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes from database: \n");
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose the number of dish you want to add ingredient in: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > dishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var dishView = dishesView.ElementAt(number - 1);

                var ingredientsDto = ingredientService.GetIngredients();
                Console.WriteLine(ingredientsDto.Count());
                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredientsDto);
                Console.WriteLine("Here is the list of all ingredients from database: \n");
                int count = 1;
                foreach (var item in ingredientsView)
                {
                    Console.WriteLine(count++);
                    Console.WriteLine($"Ingredient id: {item.IngredientId}; Ingredient name: {item.IngredientName}\n");
                }
                Console.Write("Choose the number of ingredient you want to add: ");
                int ingrNumber;
                if (!Int32.TryParse(Console.ReadLine(), out ingrNumber) || ingrNumber > ingredientsView.Count() || ingrNumber < 1)
                {
                    Console.WriteLine("The number of ingredient is not correct, try again");
                    return this;
                }
                var ingredient = ingredientsView.ElementAt(ingrNumber - 1);
                dishService.AddIngredient(dishView.DishId, ingredient.IngredientId);
                Console.WriteLine("Adding of ingredient for this dish was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteIngredientFromDish()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<DishView, DishDto>();
                    cfg.CreateMap<IngredientDto, IngredientView>();
                }).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes from database: \n");
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose the number of dish you want to delete ingredient from: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > dishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var dishView = dishesView.ElementAt(number - 1);

                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(dishService.GetIngredientsByDishId(dishView.DishId));
                Console.WriteLine("Here is the list of all ingredients for this dish: \n");
                int count = 1;
                foreach (var item in ingredientsView)
                {
                    Console.WriteLine(count++);
                    Console.WriteLine($"Ingredient id: {item.IngredientId}; Ingredient name: {item.IngredientName}\n");
                }
                Console.Write("Choose the number of ingredient you want to delete: ");
                int ingrNumber;
                if (!Int32.TryParse(Console.ReadLine(), out ingrNumber) || ingrNumber > ingredientsView.Count() || ingrNumber < 1)
                {
                    Console.WriteLine("The number of ingredient is not correct, try again");
                    return this;
                }
                var ingredient = ingredientsView.ElementAt(ingrNumber - 1);
                dishService.DeleteIngredient(dishView.DishId, ingredient.IngredientId);
                Console.WriteLine("Deleting of ingredient for this dish was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GetListOfIngredients()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg => 
                { 
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<IngredientDto, IngredientView>();
                }).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes from database: \n");
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose the number of dish you want to view ingredients of: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > dishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var dish = dishesView.ElementAt(number - 1);
                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(dishService.GetIngredientsByDishId(dish.DishId));
                Console.WriteLine("Here is the list of all ingredients for this dish: \n");
                int count = 1;
                foreach (var item in ingredientsView)
                {
                    Console.WriteLine(count++);
                    Console.WriteLine($"Ingredient id: {item.IngredientId}; Ingredient name: {item.IngredientName}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GoToMenu()
        {
            return new BaseController();
        }

        public IController GetPricesForDish()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DishDto, DishView>()).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes from database: \n");
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose the number of dish you want to view prices of: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > dishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var dish = dishesView.ElementAt(number - 1);
                return priceListController.GetPricesForDish(dish.DishId);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
        }

        protected void Dispose()
        {
            dishService.Dispose();
        }
    }
}
