﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Interfaces;
using Restaurant.Interfaces;
using Restaurant.Infrastructure;
using Restaurant.EntetiesView;
using BLL.EntetiesDTO;
using AutoMapper;

namespace Restaurant.Controllers
{
    class OrderDishesController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        private readonly IOrderDishesService orderDishesService;

        private readonly IOrderService orderService;

        private readonly IPriceListService priceService;

        public OrderDishesController()
        {
            orderDishesService = Configuration.GetService(typeof(IOrderDishesService)) as IOrderDishesService;
            orderService = Configuration.GetService(typeof(IOrderService)) as IOrderService;
            priceService = Configuration.GetService(typeof(IPriceListService)) as IPriceListService;
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetOrderDishesList,
                [2] = ChangeOrderDishesInfo,
                [3] = DeleteOrderDishes,
                [4] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - get the list of all dishes in orders" +
                "\n2 - change information about order dishes" +
                "\n3 - delete dish in order" +
                "\n4 - return to the main menu\n");
        }

        public IController GetOrderDishesList()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesDto, OrderDishesView>()).CreateMapper();
                var orderDishesView = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(orderDishesService.GetOrdersDishes());
                int num = 1;
                Console.WriteLine("Here is the list of all dishes in of orders from database: \n");
                foreach (var item in orderDishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish of order id: {item.OrderDishesId}; \nOrder id: {item.OrderId}; Price of dish: {item.PriceId}; \nCount of portion: {item.PortionCount}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again.");
                return this;
            }
            return this;
        }


        public IController ChangeOrderDishesInfo()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                    cfg.CreateMap<OrderDishesView, OrderDishesDto>();
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                }).CreateMapper();
                var ordersDishesView = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(orderDishesService.GetOrdersDishes());
                int num = 1;
                Console.WriteLine("Here is the list of all dishes in orders from database: \n");
                foreach (var item in ordersDishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish of order id: {item.OrderDishesId}; \nOrder id: {item.OrderId}; Price of dish: {item.PriceId}; \nCount of portion: {item.PortionCount}\n");
                }
                Console.Write("Choose the number of dish in order you want to change information about:   ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ordersDishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of order dish is not correct, try again");
                    return this;
                }
                var orderDishView = ordersDishesView.ElementAt(number - 1);
                var ordersView = mapper.Map<IEnumerable<OrderDto>,IEnumerable<OrderView>>(orderService.GetOrders());
                int numb = 1;
                Console.WriteLine("Here is the list of all orders from database: \n");
                foreach (var item in ordersView)
                {
                    Console.WriteLine(numb++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
                Console.Write("Choose the number of order you want to reset or '0' if don't want to change:   ");
                var orderNumber = Console.ReadLine();
                int orderNum;
                if (orderNumber != "0")
                {
                    if (!Int32.TryParse(orderNumber, out orderNum) || orderNum > ordersView.Count() || orderNum < 1)
                    {
                        Console.WriteLine("The number of order is not correct, try again");
                        return this;
                    }
                    orderDishView.OrderId = ordersView.ElementAt(number - 1).OrderId;
                }

                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(priceService.GetPrices());
                int numberPrice = 1;
                Console.WriteLine("Here is the list of all dishes with prices from database: \n");
                foreach (var item in pricesView)
                {
                    Console.WriteLine(numberPrice++);
                    Console.WriteLine($"Price id: {item.PriceId}; \nDish id: {item.DishId}; \nPortion weight: {item.Portion}; \nPrice per portion: {item.PricePerPortion}\n");
                }
                Console.Write("Choose the number of dish and price you want to reset or '0' if don't want to change:   ");
                var priceNumber = Console.ReadLine();
                int priceNum;
                if (priceNumber != "0")
                {
                    if (!Int32.TryParse(priceNumber, out priceNum) || priceNum > pricesView.Count() || priceNum < 1)
                    {
                        Console.WriteLine("The number of price is not correct, try again");
                        return this;
                    }
                    orderDishView.PriceId = pricesView.ElementAt(number - 1).PriceId;
                }

                Console.Write("Write new amount of portions or '0' if you don't want to change it:   ");
                var amount = Console.ReadLine();
                int count;
                if(amount != "0")
                {
                    if (!Int32.TryParse(amount, out count) || count > 200 || count < 1)
                    {
                        Console.WriteLine("The number of amount is not correct, try again");
                        return this;
                    }
                    orderDishView.PortionCount = count;
                }
                orderDishesService.Update(mapper.Map<OrderDishesView, OrderDishesDto>(orderDishView));
                Console.WriteLine("Changing information was successfully done!");

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController FindOrderDishesByOrderId(int orderId)
        {
            try
            {
                var orderDishesDto = orderDishesService.FindOrderDishesByOrderId(orderId);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesDto, OrderDishesView>()).CreateMapper();
                var ordersDishesView = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(orderDishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes in orders from database: \n");
                foreach (var item in ordersDishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish of order id: {item.OrderDishesId}; \nOrder id: {item.OrderId}; Price of dish: {item.PriceId}; \nCount of portion: {item.PortionCount}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("If you want to try again, write '1', else you will be navigated to main menu.");
                if(Console.ReadLine() == "1")
                {
                    return FindOrderDishesByOrderId(orderId);
                }
                return GoToMenu();
            }
            return new OrderController();
        }

        public IController DeleteOrderDishes()
        {
            try
            {
                var orderDishesDto = orderDishesService.GetOrdersDishes();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesDto, OrderDishesView>()).CreateMapper();
                var orderDishesView = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(orderDishesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all dishes in orders: ");
                foreach (var item in orderDishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish of order id: {item.OrderDishesId}; \nOrder id: {item.OrderId}; Price of dish: {item.PriceId}; \nCount of portion: {item.PortionCount}\n");
                }
                Console.Write("Choose the number of dish in order that you want to delete: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > orderDishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var orderDishView = orderDishesView.ElementAt(number - 1);
                orderDishesService.Delete(orderDishView.OrderDishesId);
                Console.WriteLine("Deleting of this dish in order was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteOrderDishesById(int orderId)
        {
            try
            {
                var allOrderDishes = orderDishesService.GetOrdersDishes();
                var orderDishesDto = allOrderDishes.Where(ord => ord.OrderId == orderId);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDishesDto, OrderDishesView>()).CreateMapper();
                var orderDishesView = mapper.Map<IEnumerable<OrderDishesDto>, IEnumerable<OrderDishesView>>(orderDishesDto);
                int num = 1;
                Console.WriteLine("Here is tle list of all dishes in this order: ");
                foreach (var item in orderDishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish of order id: {item.OrderDishesId}; \nOrder id: {item.OrderId}; Price of dish: {item.PriceId}; \nCount of portion: {item.PortionCount}\n");
                }
                Console.Write("Choose the number of dish in order you want to delete: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > orderDishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return DeleteOrderDishesById(orderId);
                }
                var orderDishView = orderDishesView.ElementAt(number - 1);
                orderDishesService.Delete(orderDishView.OrderDishesId);
                Console.WriteLine("Deleting of this order was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("If you want to try again, write '1', else you will be navigated to main menu.");
                if (Console.ReadLine() == "1")
                {
                    return DeleteOrderDishesById(orderId);
                }
                return GoToMenu();
            }
            return new OrderController();
        }


        public IController GoToMenu()
        {
            return new BaseController();
        }
    }
}
