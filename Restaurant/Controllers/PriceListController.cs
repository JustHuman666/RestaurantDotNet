﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Interfaces;
using Restaurant.Interfaces;
using Restaurant.Infrastructure;
using Restaurant.EntetiesView;
using BLL.EntetiesDTO;
using AutoMapper;

namespace Restaurant.Controllers
{
    public class PriceListController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        private readonly IPriceListService priceService;

        private readonly IDishService dishService;

        public PriceListController()
        {
            priceService = Configuration.GetService(typeof(IPriceListService)) as IPriceListService;
            dishService = Configuration.GetService(typeof(IDishService)) as IDishService;
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetPricesList,
                [2] = AddNewPrice,
                [3] = ChangePriceInfo,
                [4] = DeletePrice,
                [5] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
               "\n1 - get the list of all prices" +
               "\n2 - add new price in database" +
               "\n3 - change price about dish" +
               "\n4 - delete price from database" +
               "\n5 - return to the main menu\n");
        }

        public IController GetPricesList()
        {
            try
            {
                var pricesDto = priceService.GetPrices();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceListDto, PriceListView>()).CreateMapper();
                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(pricesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all prices from database: \n");
                foreach (var item in pricesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Price id: {item.PriceId}; \nDish id: {item.DishId}; \nPortion weight: {item.Portion}; \nPrice per portion: {item.PricePerPortion}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddNewPrice()
        {
            try
            {
                var dishesDto = dishService.GetDishes();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<PriceListView, PriceListDto>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                }).CreateMapper();
                var dishesView = mapper.Map<IEnumerable<DishDto>,IEnumerable<DishView>>(dishesDto);
                Console.WriteLine("Here is the list of all dishes from database: ");
                int num = 1;
                foreach (var item in dishesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose the number of dish you want to add prices for: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > dishesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var dish = dishesView.ElementAt(number - 1);
                Console.Write("Write the weight of new portion:   ");
                int weight;
                if (!Int32.TryParse(Console.ReadLine(), out weight))
                {
                    Console.WriteLine("The weight is not correct, try again");
                    return this;
                }
                Console.Write("Write the price for this portion: ");
                decimal price;
                if (!Decimal.TryParse(Console.ReadLine(), out price))
                {
                    Console.WriteLine("The price is not correct, try again");
                    return this;
                }
                PriceListView priceView = new PriceListView() { DishId = dish.DishId, Portion = weight, PricePerPortion = price };
                priceService.Add(mapper.Map<PriceListView, PriceListDto>(priceView));
                Console.WriteLine("Adding of price was successfully done");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GetPricesForDish(int dishId)
        {
            try
            {
                var pricesDto = priceService.GetPrices();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceListDto, PriceListView>()).CreateMapper();
                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(pricesDto).Where(pr => pr.DishId == dishId);
                int num = 1;
                Console.WriteLine("Here is the list of all prices for this dish from database: \n");
                foreach (var item in pricesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Price id: {item.PriceId}; \nDish id: {item.DishId}; \nPortion weight: {item.Portion}; \nPrice per portion: {item.PricePerPortion}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("If you want to try again, write '1', else you will be navigated to main menu.");
                if (Console.ReadLine() == "1")
                {
                    return GetPricesForDish(dishId);
                }
                return GoToMenu();
            }
            return new DishController();
        }


        public IController ChangePriceInfo()
        {
            try
            {
                var pricesDto = priceService.GetPrices();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PriceListDto, PriceListView>();
                    cfg.CreateMap<PriceListView, PriceListDto>();
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                var pricesView = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(pricesDto);
                int num = 1;
                Console.WriteLine("Here is the list of all prices from database: \n");
                foreach (var item in pricesView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Price id: {item.PriceId}; \nDish id: {item.DishId}; \nPortion weight: {item.Portion}; \nPrice per portion: {item.PricePerPortion}\n");
                }
                Console.Write("Choose the number of price you want to change information about: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > pricesView.Count() || number < 1)
                {
                    Console.WriteLine("The number of price is not correct, try again");
                    return this;
                }
                var priceView = pricesView.ElementAt(number - 1);
                var dishesView = mapper.Map<IEnumerable<DishDto>, IEnumerable<DishView>>(dishService.GetDishes());
                Console.WriteLine("Here is the list of all dishes from database: ");
                int amount = 1;
                foreach (var item in dishesView)
                {
                    Console.WriteLine(amount++);
                    Console.WriteLine($"Dish id: {item.DishId}; Dish name: {item.DishName}\n");
                }
                Console.Write("Choose an id of dish or '0' if you don't want to change it:   ");
                var choice = Console.ReadLine();
                int dishId;
                if (choice != "0")
                {
                    if (!Int32.TryParse(choice, out dishId) || dishId > dishesView.Count() || dishId < 1)
                    {
                        Console.WriteLine("The id of dish is not correct, try again");
                        return this;
                    }
                    priceView.DishId = dishId;
                }
                Console.Write("Write new weight of dish or '0' if you don't want to change it:   ");
                var weightChoice = Console.ReadLine();
                int weight;
                if (weightChoice != "0")
                {
                    if (!Int32.TryParse(Console.ReadLine(), out weight) || weight < 0)
                    {
                        Console.WriteLine("The weight is not correct, try again");
                        return this;
                    }
                    priceView.Portion = weight;
                }
                Console.Write("Write price of dish or '0' if you don't want to change it:   ");
                int pricePerPortion;
                var priceChoice = Console.ReadLine();
                if(priceChoice != "0")
                {
                    if (!Int32.TryParse(priceChoice, out pricePerPortion) || pricePerPortion < 0)
                    {
                        Console.WriteLine("The number of price is not correct, try again");
                        return this;
                    }
                    priceView.PricePerPortion = pricePerPortion;
                }
                var priceListDto = mapper.Map<PriceListView, PriceListDto>(priceView);
                priceService.Update(priceListDto);
                Console.WriteLine("Changing information was successfully done!");

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeletePrice()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PriceListDto, PriceListView>()).CreateMapper();
                var pricesViews = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(priceService.GetPrices());
                int num = 1;
                Console.WriteLine("Here is the list of all prices from database: \n");
                foreach (var item in pricesViews)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Price id: {item.PriceId}; \nDish id: {item.DishId}; \nPortion weight: {item.Portion}; \nPrice per portion: {item.PricePerPortion}\n");
                }
                Console.Write("Choose the number of price you want to delete: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > pricesViews.Count() || number < 1)
                {
                    Console.WriteLine("The number of price is not correct, try again");
                    return this;
                }
                var priceView = pricesViews.ElementAt(number - 1);
                priceService.Delete(priceView.PriceId);
                Console.WriteLine("Deleting of this price was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GoToMenu()
        {
            return new BaseController();
        }
        
        protected void Dispose()
        {
            priceService.Dispose();
        }
    }
}
