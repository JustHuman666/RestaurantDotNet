﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Interfaces;
using Restaurant.Interfaces;
using Restaurant.Infrastructure;
using Restaurant.EntetiesView;
using BLL.EntetiesDTO;
using AutoMapper;

namespace Restaurant.Controllers
{
    public class OrderController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        private readonly IOrderService orderService;

        private readonly IOrderDishesService orderDishesService;

        private readonly IDishService dishService;

        private readonly IPriceListService priceService;

        private readonly OrderDishesController orderDishesContr;

        public OrderController()
        {
            orderService = Configuration.GetService(typeof(IOrderService)) as IOrderService;
            dishService = Configuration.GetService(typeof(IDishService)) as IDishService;
            priceService = Configuration.GetService(typeof(IPriceListService)) as IPriceListService;
            orderDishesService = Configuration.GetService(typeof(IOrderDishesService)) as IOrderDishesService;
            orderDishesContr = new OrderDishesController();
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetOrdersList,
                [2] = AddNewOrder,
                [3] = ChangeOrderInfo,
                [4] = FindOrdersByCustomer,
                [5] = DeleteOrder,
                [6] = AddDishToOrder,
                [7] = DeleteDishFromOrder,
                [8] = GetListOfDishes,
                [9] = GetTotalPrice,
                [10] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - get the list of all orders" +
                "\n2 - add new order in database" +
                "\n3 - change information about order" +
                "\n4 - find order for customer" +
                "\n5 - delete order from database" +
                "\n6 - add dish in order" +
                "\n7 - delete dish from order" +
                "\n8 - get the list of all dishes in one order" +
                "\n9 - get total price for one order" +
                "\n10 - return to the main menu\n");
        }

        public IController GetOrdersList()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDto, OrderView>()).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(orderService.GetOrders());
                int num = 1;
                Console.WriteLine("Here is the list of all orders from database: \n");
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddNewOrder()
        {
            try
            {
                Console.Write("Write the name of customer you want to add:   ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                OrderView orderView = new OrderView() { CustomerName = name, OrderDate = DateTime.Now};
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderView, OrderDto>()).CreateMapper();
                var orderDto = mapper.Map<OrderView, OrderDto>(orderView);
                orderService.Add(orderDto);
                Console.WriteLine("Order was added\n");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController ChangeOrderInfo()
        {
            try
            {
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderView, OrderDto>();
                }).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(orderService.GetOrders());
                int num = 1;
                Console.WriteLine("Here is the list of all orders from database: \n");
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
                Console.Write("Choose the number of order you want to change information about: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ordersView.Count() || number < 1)
                {
                    Console.WriteLine("The number of order is not correct, try again");
                    return this;
                }
                var orderView = ordersView.ElementAt(number - 1);
                Console.Write("Write new name of customer or '0' if you don't want to change it:   ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                Console.Write("Write 'yes' if you want to change date and time for order for current value, or any other if you don't want:   ");
                string choice = Console.ReadLine();
                if(name != "0")
                {
                    orderView.CustomerName = name;
                }
                if (choice == "yes")
                {
                    orderView.OrderDate = DateTime.Now;
                }
                var orderDto = mapper.Map<OrderView, OrderDto>(orderView);
                orderService.Update(orderDto);
                Console.WriteLine("Changing information was successfully done!");

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController FindOrdersByCustomer()
        {
            try
            {
                Console.Write("Write the name of customer you want to find:   ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                var ordersDto = orderService.FindOrdersByCustomer(name);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDto, OrderView>()).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                Console.WriteLine($"Here is the list of all orders of customer with name: {name}: \n");
                int num = 1;
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteOrder()
        {
            try
            {
                var ordersDto = orderService.GetOrders();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDto, OrderView>()).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                int num = 1;
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
                Console.Write("Choose the number of order you want to delete: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ordersView.Count() || number < 1)
                {
                    Console.WriteLine("The number of order is not correct, try again");
                    return this;
                }
                var orderView = ordersView.ElementAt(number - 1);
                orderService.Delete(orderView.OrderId);
                Console.WriteLine("Deleting of this order was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddDishToOrder()
        {
            try
            {
                var ordersDto = orderService.GetOrders();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderView, OrderDto>();
                    cfg.CreateMap<DishDto, DishView>();
                    cfg.CreateMap<PriceListDto, PriceListView>();
                    cfg.CreateMap<OrderDishesDto, OrderDishesView>();
                }).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                int num = 1;
                Console.WriteLine("Here is the list of all orders from database: \n");
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
                Console.Write("Choose the number of order you want to add dish in: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ordersView.Count() || number < 1)
                {
                    Console.WriteLine("The number of order is not correct, try again");
                    return this;
                }
                var orderView = ordersView.ElementAt(number - 1);
                var prices = mapper.Map<IEnumerable<PriceListDto>, IEnumerable<PriceListView>>(priceService.GetPrices());
                Console.WriteLine("Here is the list of all dishes and prices from database: \n");
                int count = 1;
                foreach (var item in prices)
                {
                    var dishTemp = dishService.GetDishes().FirstOrDefault(d => d.DishId == item.DishId);
                    Console.WriteLine(count++);
                    Console.WriteLine($"Price id: {item.PriceId}; \nDish name: {dishTemp.DishName}; \nPortion weight: {item.Portion}; \nPrice per portion: {item.PricePerPortion}\n");
                }
                Console.Write("Choose the number of dish you want to add: ");
                int priceNum;
                if (!Int32.TryParse(Console.ReadLine(), out priceNum) || priceNum > prices.Count() || priceNum < 1)
                {
                    Console.WriteLine("The number of dish is not correct, try again");
                    return this;
                }
                var priceView = prices.ElementAt(priceNum - 1);

                Console.Write("Write the amount of portions you want to add:   ");
                int amount;
                if (!Int32.TryParse(Console.ReadLine(), out amount))
                {
                    Console.WriteLine("This number of amount is not correct, try again");
                    return this;
                }
                orderService.AddDishToOrder(orderView.OrderId, priceView.PriceId, amount);

                Console.WriteLine("Adding of dish to order was successfully done!");

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteDishFromOrder()
        {
            try
            {
                var ordersDto = orderService.GetOrders();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderDto, OrderView>()).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                int num = 1;
                Console.WriteLine("Here is the list of all orders from database: \n");
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
                Console.Write("Choose the number of order you want to delete dish from: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ordersView.Count() || number < 1)
                {
                    Console.WriteLine("The number of order is not correct, try again");
                    return this;
                }
                var orderView = ordersView.ElementAt(number - 1);
                return orderDishesContr.DeleteOrderDishesById(orderView.OrderId);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
        }

        public IController GetListOfDishes()
        {
            try
            {
                var ordersDto = orderService.GetOrders();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderView, OrderDto>();
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                int num = 1;
                Console.WriteLine("Here is the list of all orders from database: \n");
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
                Console.Write("Choose the number of order you want to view dishes from: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ordersView.Count() || number < 1)
                {
                    Console.WriteLine("The number of order is not correct, try again");
                    return this;
                }
                var orderView = ordersView.ElementAt(number - 1);
                return orderDishesContr.FindOrderDishesByOrderId(orderView.OrderId);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
        }

        public IController GoToMenu()
        {
            return new BaseController();
        }

        public IController GetTotalPrice()
        {
            try
            {
                var ordersDto = orderService.GetOrders();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<OrderDto, OrderView>();
                    cfg.CreateMap<OrderView, OrderDto>();
                    cfg.CreateMap<DishDto, DishView>();
                }).CreateMapper();
                var ordersView = mapper.Map<IEnumerable<OrderDto>, IEnumerable<OrderView>>(ordersDto);
                int num = 1;
                Console.WriteLine("Here is the list of all orders from database: \n");
                foreach (var item in ordersView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Order id: {item.OrderId}; \nCustomer name: {item.CustomerName}; \nDate and time of creating of order: {item.OrderDate}\n");
                }
                Console.Write("Choose the number of order you want to view dishes from: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ordersView.Count() || number < 1)
                {
                    Console.WriteLine("The number of order is not correct, try again");
                    return this;
                }
                var orderView = ordersView.ElementAt(number - 1);
                var orderDishesDto = orderDishesService.GetOrdersDishes().Where(ord => ord.OrderId == orderView.OrderId);
                decimal totalPrice = 0;
                foreach (var item in orderDishesDto)
                {
                    var price = priceService.GetPrices().FirstOrDefault(pr => pr.PriceId == item.PriceId);
                    totalPrice += item.PortionCount * price.PricePerPortion;
                }
                orderView.TotalPrice = totalPrice;
                orderService.Update(mapper.Map<OrderView, OrderDto>(orderView));
                Console.WriteLine($"Total price for this order: {orderView.TotalPrice}");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        protected void Dispose()
        {
            orderService.Dispose();
        }
    }
}
