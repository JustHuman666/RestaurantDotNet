﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Interfaces;
using Restaurant.Interfaces;
using Restaurant.Infrastructure;
using Restaurant.EntetiesView;
using BLL.EntetiesDTO;
using AutoMapper;

namespace Restaurant.Controllers
{
    public class IngredientController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        private readonly IIngredientService ingredientService;

        public IngredientController()
        {
            ingredientService = Configuration.GetService(typeof(IIngredientService)) as IIngredientService;
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetIngredientsList,
                [2] = AddIngredient,
                [3] = ChangeIngredientInfo,
                [4] = FindIngredient,
                [5] = DeleteIngredient,
                [6] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - get the list of all ingredients" +
                "\n2 - add new ingredient in database" +
                "\n3 - change information about ingredient" +
                "\n4 - find ingredient" +
                "\n5 - delete ingredient from database" +
                "\n6 - return to the main menu\n");
        }

        public IController GetIngredientsList()
        {
            try
            {
                var ingredientsDto = ingredientService.GetIngredients();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap< IngredientDto, IngredientView>()).CreateMapper();
                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredientsDto);
                int num = 1;
                Console.WriteLine("Here is the list of all ingredients from database: \n");
                foreach (var item in ingredientsView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Ingredient id: {item.IngredientId}; Ingredient name: {item.IngredientName}\n");
                }
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddIngredient()
        {
            try
            {
                Console.Write("Write the name of ingredient you want to add:   ");
                string name = Console.ReadLine();
                if(name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                IngredientView ingredientView = new IngredientView() { IngredientName = name };
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientView, IngredientDto>()).CreateMapper();
                var ingredientDto = mapper.Map<IngredientView, IngredientDto>(ingredientView);
                ingredientService.Add(ingredientDto);
                Console.WriteLine("Ingredient was added\n");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController ChangeIngredientInfo()
        {
            try
            {
                var ingredientsDto = ingredientService.GetIngredients();
                var mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<IngredientDto, IngredientView>();
                    cfg.CreateMap<IngredientView, IngredientDto>();
                }).CreateMapper();
                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredientsDto);
                int num = 1;
                Console.WriteLine("Here is the list of all ingredients from database: \n");
                foreach (var item in ingredientsView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Ingredient id: {item.IngredientId}; Ingredient name: {item.IngredientName}\n");
                }
                Console.Write("Choose the number of product you want to change information about: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ingredientsView.Count() || number < 1)
                {
                    Console.WriteLine("The number of ingredient is not correct, try again");
                    return this;
                }
                var ingredientView = ingredientsView.ElementAt(number - 1);
                Console.Write("Write new name of ingredient or '0' if you don't want to change it:   ");
                string name = Console.ReadLine();
                if(name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                if(name != "0")
                {
                    ingredientView.IngredientName = name;
                }
                var ingredientDto = mapper.Map<IngredientView, IngredientDto>(ingredientView);
                ingredientService.Update(ingredientDto);
                Console.WriteLine("Changing information was successfully done!");

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController FindIngredient()
        {
            try
            {
                Console.Write("Write the name of ingredient you want to find:   ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("The name cannot be empty!\n");
                    return this;
                }
                var ingredientDto = ingredientService.FindByName(name);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, IngredientView>()).CreateMapper();
                var ingredientView = mapper.Map<IngredientDto, IngredientView>(ingredientDto);
                Console.WriteLine($"Ingredient id: {ingredientView.IngredientId}; Ingredient name: {ingredientView.IngredientName}\n");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteIngredient()
        {
            try
            {
                var ingredientsDto = ingredientService.GetIngredients();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<IngredientDto, IngredientView>()).CreateMapper();
                var ingredientsView = mapper.Map<IEnumerable<IngredientDto>, IEnumerable<IngredientView>>(ingredientsDto);
                int num = 1;
                Console.WriteLine("Here is the list of all ingredients from database: \n");
                foreach (var item in ingredientsView)
                {
                    Console.WriteLine(num++);
                    Console.WriteLine($"Ingredient id: {item.IngredientId}; Ingredient name: {item.IngredientName}\n");
                }
                Console.Write("Choose the number of product you want to delete: ");
                int number;
                if (!Int32.TryParse(Console.ReadLine(), out number) || number > ingredientsView.Count() || number < 1)
                {
                    Console.WriteLine("The number of ingredient is not correct, try again");
                    return this;
                }
                var ingredientView = ingredientsView.ElementAt(number - 1);
                ingredientService.Delete(ingredientView.IngredientId);
                Console.WriteLine("Deleting of this ingredient was successfully done!");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GoToMenu()
        {
            return new BaseController();
        }

        protected void Dispose()
        {
            ingredientService.Dispose();
        }
    }
}
