﻿using System;
using System.Collections.Generic;

namespace Restaurant.Interfaces
{
    public interface IController
    {
        Dictionary<int, Func<IController>> Controllers { get; set; }

        void ShowMenu();

    }
}
