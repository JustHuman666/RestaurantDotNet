﻿using System;
using BLL.Infrastructure;
using Ninject.Modules;
using Ninject;

namespace Restaurant.Infrastructure
{
    /// <summary>
    /// Static class for configuration of program
    /// </summary>
    public static class Configuration
    {
        /// <summary>
        /// Connection string of DB
        /// </summary>
        private static readonly string ConectionString = @"Server = localhost, 1433; Database = RestaurantDB; User ID = sa; Password = <password12345>";
        
        /// <summary>
        /// The container of services
        /// </summary>
        private static NinjectModule Service = new IOCUoWModule(ConectionString);

        /// <summary>
        /// The container of unit of work
        /// </summary>
        private static NinjectModule UoW = new IOCServiceModule();

        /// <summary>
        /// Create standart kernel for containers
        /// </summary>
        private static StandardKernel Kernel = new StandardKernel(Service, UoW);

        /// <summary>
        /// To get the service of given type from container
        /// </summary>
        /// <param name="serviceType">type of interface of service</param>
        /// <returns>the instance of service of given type</returns>
        public static object GetService(Type serviceType)
        {
            return Kernel.TryGet(serviceType);
        }
    }
}
