﻿using System;
using System.Collections.Generic;
using Restaurant.Controllers;
using Restaurant.Interfaces;

namespace Restaurant
{
    class Program
    {
        static void Main()
        {
            IController controller = new BaseController();
            while (true)
            {
                int key;
                controller.ShowMenu();
                if (int.TryParse(Console.ReadLine(), out key) && controller.Controllers.ContainsKey(key))
                    controller = controller.Controllers[key].Invoke();
            }

        }
    }
}
